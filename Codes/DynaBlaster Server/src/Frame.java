import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.util.Date;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class Frame  extends JPanel implements ActionListener{
	public static Frame frame;
	static Config cfg = Config.getInstance();
	private String[] language;
	private static String[] languages;
	 static int languageIndex;
	private boolean online;
	private Connection connection;
	private JPanel info;
	private JScrollPane scroll;
	public JButton start;
	public JTextField port_;
	
	public Frame(){
		
		frame = this;
		language = cfg.getLanguage(0);
		languageIndex= 0;
		languages = new String[cfg.getLangugagesNumber()];
			for(int i=0; i<cfg.getLangugagesNumber(); i++){
				languages[i] = cfg.getLanguagesNames(i);
			}
			
		JComboBox languagesList = new JComboBox(languages);		
		languagesList.setSelectedIndex(0);
		languagesList.addActionListener(this);
		languagesList.setPreferredSize(new Dimension(70, 30));	
			
		JPanel N = new JPanel();
		JPanel S = new JPanel();
		JPanel button = new JPanel();
		JPanel port = new JPanel();
		start = new JButton(language[1]);
		JButton exit = new JButton(language[2]);
		JLabel portText= new JLabel(language[3]);
		JLabel ip;
		JLabel selectLanguage = new JLabel(language[0] + ":   ");
		selectLanguage.setFont(new Font("Serif", Font.BOLD, 12));
		selectLanguage.setBackground(Color.green);
		
		port_ = new JTextField(Integer.toString(cfg.port));
		port_.setSize(200,200);
		try {
			 ip = new JLabel(language[4] + ":   "
					+ InetAddress.getLocalHost().getHostAddress());
		} catch (UnknownHostException e) {
			 addMessage(language[5], true);
			 ip = new JLabel(language[4] + ":   "+ language[5]);
		}
		start.setFocusable(false);
		exit.setFocusable(false);
		
		info = new JPanel();
		scroll = new JScrollPane(info);
		setLayout(new BorderLayout());
		info.setLayout(new BoxLayout(info, BoxLayout.Y_AXIS));
		button.add(start);
		button.add(exit);
		port.add(portText);
		port.add(port_);
		N.add(ip, BorderLayout.WEST);
		N.add(selectLanguage, BorderLayout.EAST);
		N.add(languagesList, BorderLayout.CENTER);

		S.add(port, BorderLayout.EAST);
		S.add(button, BorderLayout.WEST);
		add(scroll,BorderLayout.CENTER);
		add(S, BorderLayout.SOUTH);
		add(N, BorderLayout.NORTH);
		
		languagesList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				languageIndex = languagesList.getSelectedIndex();
				language = cfg.getLanguage(languageIndex);
				exit.setText(language[2]);
				selectLanguage.setText(language[0] + ":   ");
			}
		});
		start.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if(online){
					connection.interrupt();
					setEnabled();
				}
				else{
					try{
						int port = Integer.parseInt(port_.getText());
						start.setText(language[6]);
						port_.setEnabled(false);
						online=true;
						try{
							connection = new Connection(port);
						}
						catch(Exception f){}
					}
					catch (NumberFormatException g) {
						addMessage(language[2], true);
					}
				}
			}
			
		});
		
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				System.exit(0);
			}
		});
		
		info.addContainerListener(new ContainerListener() {
			public void componentAdded(ContainerEvent e) {
				scroll.getVerticalScrollBar().setValue(
						scroll.getVerticalScrollBar().getMaximum());
			}
			public void componentRemoved(ContainerEvent e) {
			}
		});
		
	} 
	@Override
	public void actionPerformed(ActionEvent arg0) {
	} 
	
	/**
	 * metoda wypisująca wiadomości w oknie dialogowym
	 * @param message
	 * @param time
	 */
	public void addMessage(String message, boolean time) {
		JLabel label;
		if (time) {
			label = new JLabel(" ["
					+ DateFormat.getDateTimeInstance().format(new Date())
					+ "] " + message);
		} else {
			label = new JLabel(message);
		}
		info.add(label);
		info.revalidate();
	}
	
	/**
	 * zmienia stan serwera na nieaktywny
	 */
	public void setEnabled() {
		start.setText(language[1]);	
		port_.setEnabled(true);
		online = false;
	}
}
