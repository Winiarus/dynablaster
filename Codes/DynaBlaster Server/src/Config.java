import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**Klasa odpowiedzialna za wczytywanie z plik�w konfiguracyjnych
 * 
 * @author Dariusz B�aszkiewicz
 * @author Micha� Winiarek
 */
public final class Config {
	
	private final static Config instance = new Config();
	public static Config getInstance(){return instance;}

	public String[] languagesList;
	public String[] language;
	
	public int time;
	private int defaultTime = 10;
	private int defaultPort = 6000;
	public int port = getValue("Default Port", defaultPort);
	private String[] defaultLanguagesList = {"Default"};
	private String[] defaultLanguage = {"Select Language", "Start", "Exit", "Port", 
		"Serwer IP","Unknown" ,"Stop", "Server is ready to work on port", 
		"Port number is not suitable", "Server Error", "Cannot read best scores from file",
		"Cannot save high scores to file", "New score have been saved",
		"Waiting for score", "Socket timeout","Sending high scores","Input/output exception",
		"Is already in use", "Data sent by client is incorrect", "Null pointer"
	
		};
	
	private Config(){  
		//time = getValue("#Time", defaultTime);
		port = getValue("#Default Port", defaultPort);
		languagesList = getLanguagesList();
	}
	
	/**Zamiana stringa z pliku na int
	 * 
	 * @param key klucz dla porz�danej liczby
	 * @param defaultValue warto��, przy nieudanym wczytaniu pliku
	 * 
	 * @return liczba dla danego klucza
	 */
	int getValue(String key, int defaultValue){
		
		String tmp = fromFile(key);
		int tmpValue;
		if(tmp == null){
			return defaultValue;
		}
		try{
			tmpValue = Integer.parseInt(tmp);
		} catch (NumberFormatException e){
			return defaultValue;
		}
		return tmpValue;
	}
	
	/**Pobieranie listy j�zyk�w
	 * 
	 * @return tablica j�zyk�w	  
	 */
	private String[] getLanguagesList(){
		
		File file = new File("Languages/languagesList.txt");
		Scanner in = null;
		try{
			in = new Scanner(file);
		} catch(FileNotFoundException e){
			return defaultLanguagesList;
		}
		String[] tmp = new String[99];
		int i = 1;
		tmp[0] = "Default";
		try{
			in.nextLine();
			while (true) {
				tmp[i] = in.nextLine();
				i++;
			}
		}catch(NoSuchElementException e){
			String[] lang = new String[i];
			for (int j = 0; j < i; j++)
				lang[j] = tmp[j];
			try {
				in.close();
			} catch (IllegalStateException ise) {
			}
			return lang;
		}catch (IllegalStateException e) {
			try {
				in.close();
			} catch (IllegalStateException ise) {
			}
			return defaultLanguagesList;
		}
	}

	/**Pobieranie oczekiwanej warto�ci z pliku konfiguracyjnego
	 * 
	 * @param key klucz dla porz�danego tekstu
	 * @return porz�dany tekst
	 */
	private String fromFile(String key){
		File file = new File("config.txt");
		Scanner in = null;
		try{
			in = new Scanner(file);
		} catch (FileNotFoundException e){
			return null;
		}
		String value = null;
		try{
			while(true){
				value = in.nextLine();
				if(value.equals(key)){
					value = in.nextLine();
					break;
				}
			}
		} catch(NoSuchElementException e){
			try{
				in.close();
			} catch(IllegalStateException ise){	
			}
			return null;
		} catch (IllegalStateException e) {
			try {
				in.close();
			} catch (IllegalStateException ise) {
			}
			return null;
		} 
		try{
			in.close();
		} catch(IllegalStateException ise){	
		}
		return value;
	}
	
	/** Wczytywanie mapy z pliku
	 * 
	 * @param lvl poziom gry
	 * @param height ilo�� obiekt�w mapy w pionie 
	 * @param width ilo�� obiekt�w mapy w poziomie 
	 */
	public String[] mapFromFile(int lvl, int height){
		
		File file = new File("config.txt");
		String key = "#Lvl"+lvl+"Map";
		String map[] = new String[height];
		Scanner in = null;
		try{
			in = new Scanner(file);
		} catch (FileNotFoundException e){
		}
		String value = null;
		
		try{
			while(true){
				value = in.nextLine();
				if(value.equals(key)){
					for(int h=0; h<height; h++){
						map[h] = in.nextLine();
					}
					break;
				}
			}
		} catch(NoSuchElementException e){
			try{
				in.close();
			} catch(IllegalStateException ise){	
			}
		} catch (IllegalStateException e) {
			try {
				in.close();
			} catch (IllegalStateException ise) {
			}
		} 
		try{
			in.close();
		} catch(IllegalStateException ise){	
		}
		return map;
		
	}
	
	/**Wczytywanie j�zyka z pliku
	 * 
	 * @param index numer odpowiadaj�cy danemu j�zykowi
	 * @return wyrazy w danym j�zyku
	 */
	private String[] getLanguageFromFile(int index){
		if(index == 0)
			return defaultLanguage;
		File file = new File("Languages/" + languagesList[index] + ".txt");
		Scanner in = null;
		int count = defaultLanguage.length;
		try{
			in = new Scanner(file);
		}catch(FileNotFoundException e){
			System.out.println("FileNotFoundException");//
			return defaultLanguage;
		}
		String[] tmp = new String[count];
		try{
			for(int i=0; i<count; i++){
				tmp[i] = in.nextLine();
			}
		}catch (NoSuchElementException e) {
			try {
				System.out.println("NoSuchElementException");//
				in.close();
			} catch (IllegalStateException ise) {
			}
			return defaultLanguage;
		} catch (IllegalStateException e) {
			try {
				System.out.println("IllegalStateException");//
				in.close();
			} catch (IllegalStateException ise) {
			}
			return defaultLanguage;
		}
		try {
			in.close();
		} catch (IllegalStateException ise) {
			System.out.println("IllegalStateException");//
		}
		return tmp;
	}
	
	/**Metoda zwracaj�ca j�zyk 
	 * 
	 * @param index numer odpowiadaj�cy danemu j�zykowi 
	 * @return wyrazy w danym j�zyku
	 */
	public String[] getLanguage(int index){
		return getLanguageFromFile(index);
	}
	
	/**Metoda zwracaj�ca nazw� j�zyka
	 * 
	 * @param index numer odpowiadaj�cy danemu j�zykowi 
	 * @return nazwa j�zyka
	 */
	public String getLanguagesNames(int index){
		return languagesList[index];
	}
	
	/**Metoda zwracaj�ca tablic� dost�pnych j�zyk�w
	 * 
	 * @return tablica j�zyk�w
	 */
	public String[] getLanguagesNames(){
		return getLanguagesList();
	}
	
	/**Metoda zwracaj�ca rekord dla podanego klucza
	 * 
	 * @param key klucz
	 * @return rekord
	 */
	public String getFromFile(String key){
		return fromFile(key);
	}
	
	/**Funkcja zwracaj�ca liczb� dost�pnych j�zyk�w
	 * 
	 * @return liczba j�zyk�w
	 */
	public int getLangugagesNumber(){
		return getLanguagesList().length;
	}
	/*
	public String[] getLevel(String lvl){
		File file = new File("Level.txt");
		Scanner in = null;
		try{
			in = new Scanner(file);
		}
		catch(FileNotFoundException e){
			Frame.frame.addMessage(language[2], true);// zmieni� lang
			return null;
		}
	}*/
	
	/**
	 * Metoda zwracaj�ca najlepsze wyniki
	 * @return
	 */
	public String[] getHighScores(){
		int count = 20;
		File file = new File("Scores/HighScores.txt");
		Scanner in = null;
		try{
			in = new Scanner(file);
		}catch(FileNotFoundException e){
			return null;
		}
		String[] tmp = new String[count];
		try{
			for(int i=0; i<count; i++){
				tmp[i] = in.nextLine();
			}
		}catch (NoSuchElementException e) {
			try {
				in.close();
			} catch (IllegalStateException ise) {
			}
			return null;
		} catch (IllegalStateException e) {
			try {
				in.close();
			} catch (IllegalStateException ise) {
			}
			return null;
		}
		try {
			in.close();
		} catch (IllegalStateException ise) {
		}
		return tmp;
	}
}
