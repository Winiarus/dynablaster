import java.io.IOException;
import java.net.ServerSocket;
import java.net.SocketException;

/**Klasa odpowiedzialna za po��czenie
 * 
 * @author Dariusz B�aszkiewicz
 * @author Micha� Winiarek
 */
public class Connection extends Thread {
	static Config cfg = Config.getInstance();
	private static String[] languages;
	 static int languageIndex;
	public String[] language;
	
	public ServerSocket serverSocket;
	public int port_;
	private boolean online_;
	
	public Connection(int port) {
		language = cfg.getLanguage(Frame.languageIndex);
		port_ = port;
		online_ = true;
		start();
	}
	public void run(){
		try{	
			serverSocket = new ServerSocket(port_);
			Frame.frame.addMessage(language[7]+" "+ port_, true);
		}
		catch(IOException e){
			Frame.frame.addMessage(language[3]+" "+port_+" "+
					language[17],true);
			Frame.frame.setEnabled();
			return;
		}
		catch(IllegalArgumentException e){
			Frame.frame.addMessage(language[8],true);
			Frame.frame.setEnabled();
			return;
		}
		catch(NullPointerException e){
			Frame.frame.addMessage(language[8],true); // doda� do konfiaga obs�uge languiagle[2313]
		}
		while(online_){
			try{
				new Data(serverSocket.accept());
			}
			catch(SocketException e){}
			catch(IOException e ){
				Frame.frame.addMessage(language[9], true);
			}
		}
	}
	public void interrupt() {
		try{
			online_ = false;
			serverSocket.close();
		}
		catch(IOException e){}
	}

}
