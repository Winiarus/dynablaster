import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Vector;

/**
 * Klasa odpowiadająca za komunikację z klientem
 * @author Dariusz Błaszkiewicz
 * @author Michał Winiarek
 */
public class Data extends Thread {
	private Socket socket;
	private BufferedReader br;
	private PrintWriter pw;
	public String[] language;
	static Config cfg = Config.getInstance();
	
	/**
	 * konstruktor parametrowy
	 * @param socket_
	 * @throws IOException
	 */
	public Data(Socket socket_) throws IOException{
		language = cfg.getLanguage(Frame.languageIndex);
		socket = socket_;
		InputStream is = socket_.getInputStream();
		br = new BufferedReader(new InputStreamReader(is));
		OutputStream os = socket_.getOutputStream();
		pw = new PrintWriter(os, true);
		start();
	}
	
	public void run(){
		try{
			String command = br.readLine();
			Frame.frame.addMessage("Od " +socket.getInetAddress().getHostAddress()
					+ ": "+ command, true);
			switch(command){
			case Protocol.CHECK:{
				pw.println(Protocol.CHECKED);
				Frame.frame.addMessage("Do "+ socket.getInetAddress().getHostAddress()
						+ ":" + Protocol.CHECKED ,true);
				socket.close();
				break;
			}
			case Protocol.SENDINGSCORE: {
				
				Frame.frame.addMessage(language[13],true);
				try{
					socket.setSoTimeout(cfg.time);
					String name = br.readLine();
					Frame.frame.addMessage("Od "+socket.getInetAddress().getHostAddress() +
							": "+ name, true);
					String score = br.readLine();
					Frame.frame.addMessage("Od "+socket.getInetAddress().getHostAddress() +
							": "+ score, true);
					saveScore(name, score);
				}
				catch(SocketTimeoutException e){
					socket.setSoTimeout(0);
					Frame.frame.addMessage(language[14], true);
				}
				socket.close();
				break;
			}
			case Protocol.GETHIGHSCORES:{
				
				try {
					Thread.sleep(50);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				
				String[] answer = cfg.getHighScores();
				if(answer == null){
					pw.println(Protocol.ERROR);
					Frame.frame.addMessage(language[10],true);
					Frame.frame.addMessage("Do: "+ socket.getInetAddress().getHostAddress() 
							+ ": "+ Protocol.ERROR,true);
					socket.close();
					break;
				}
				Frame.frame.addMessage(language[15],true);
				for(int i=0; i<20; i++){
					pw.println(answer[i]);
					Frame.frame.addMessage("Do: "+ socket.getInetAddress().getHostAddress() + ": "
							+ answer[i], true);
				}
				socket.close();
				break;
			}
			case Protocol.GETCONFIG:{
				String[] key = {"#Time","#NumberOfBombs","#RangeBomb","#PointsBox","#PointsOrange","#PointsGreen","#PointsBlue"};
				String[] answer = new String[key.length];
				for(int i=0; i<key.length ;++i)
					answer[i] = Integer.toString(cfg.getValue(key[i], 0));
				for(int i=0; i<key.length; i++){
					pw.println(answer[i]);
				}
				socket.close();
				break;
			}			
			case Protocol.GETMAP:{
				String header = br.readLine();
				String[] key ={
						"#Lvl"+header+"Width", 
						"#Lvl"+header+"Height", 
						"#Lvl"+header+"NumberOfBlueSpirit", 
						"#Lvl"+header+"NumberOfOrangeSpirit", 
						"#Lvl"+header+"NumberOfGreenSpirit"};
				Vector<String> vector = new Vector<>();
				
				vector.add(Integer.toString(cfg.getValue(key[0], 0)));
				vector.add(Integer.toString(cfg.getValue(key[1], 0)));
				vector.add(Integer.toString(cfg.getValue(key[2], 0)));
				
				for(int i=0; i<cfg.getValue("#Lvl"+header+"NumberOfBlueSpirit", 0); i++){
					vector.add(Integer.toString(cfg.getValue("#Lvl"+header+"BlueSpirit"+i+"X",0)));
					vector.add(Integer.toString(cfg.getValue("#Lvl"+header+"BlueSpirit"+i+"Y",0)));
				}
				vector.add(Integer.toString(cfg.getValue(key[3], 0)));	
				for(int i=0; i<cfg.getValue("#Lvl"+header+"NumberOfOrangeSpirit", 0); i++){
					vector.add(Integer.toString(cfg.getValue("#Lvl"+header+"OrangeSpirit"+i+"X",0)));
					vector.add(Integer.toString(cfg.getValue("#Lvl"+header+"OrangeSpirit"+i+"Y",0)));
				}
				vector.add(Integer.toString(cfg.getValue(key[4], 0)));
				for(int i=0; i<cfg.getValue("#Lvl"+header+"NumberOfGreenSpirit", 0); i++){
					vector.add(Integer.toString(cfg.getValue("#Lvl"+header+"GreenSpirit"+i+"X",0)));
					vector.add(Integer.toString(cfg.getValue("#Lvl"+header+"GreenSpirit"+i+"Y",0)));
				}
				
				String[] map = cfg.mapFromFile(Integer.parseInt(header), cfg.getValue("#Lvl"+header+"Height", 0));
				
				for(int i=0; i<map.length; i++){
					vector.add(map[i]);
				}
				
				String number = Integer.toString(vector.size());
				pw.println(number);
				
				String [] answer= new String[vector.size()];
				
				
				for(int i=0; i<vector.size();i++){
					answer[i]=vector.get(i);
				}
				
				for(int i=0; i<answer.length; i++){
					pw.println(answer[i]);
				}
				socket.close();
				break;
			} 	
		}
	}
		catch(IOException e){
			Frame.frame.addMessage(language[17],true);
		}
	}
	
	public void saveScore(String name, String score){
		File file = new File("Scores/HighScores.txt");
		Scanner in = null;
		try{
			in = new Scanner(file);
		}
		catch(FileNotFoundException e){
			Frame.frame.addMessage(language[10], true);
			return;
		}
		String[] names = new String[10];
		String[] scores = new String[10];
		try{
			for(int i=0; i<10; i++){
				names[i] = in.nextLine();
				scores[i] = in.nextLine();
				}
			}
		catch(NoSuchElementException e){
			Frame.frame.addMessage(language[10], true);
			try{ in.close();}
			catch(IllegalStateException ise){}
			return;
		}
		catch(IllegalStateException e){
			Frame.frame.addMessage(language[10], true);
			try{ in.close();}
			catch(IllegalStateException ise){}
			return;
		}
		try{ in.close();}
		catch(IllegalStateException e){}
		try{
			int high = Integer.parseInt(score);
			for(int i=0; i<10; i++){
				if(Integer.parseInt(scores[i])<high){
					for(int j =9; j>i; j--){
						scores[j] = scores[j-1];
						names[j] = names[j-1];
					}
					scores[i] = score;
					names[i] = name;
					break;
				}
				if(i==9){return;}
			}
		}
		catch(NumberFormatException e){
			Frame.frame.addMessage(language[18],true);
			return;
		}
		PrintWriter save = null;
		try{
			save = new PrintWriter("Scores/HighScores.txt");
			for(int i=0; i<10; i++){
				save.println(names[i]);
				save.println(scores[i]);
			}	
			save.close();
			Frame.frame.addMessage(language[12], true);
		}
		catch(FileNotFoundException e){
			Frame.frame.addMessage(language[11], true);
		}
		catch(IllegalStateException e ){
			Frame.frame.addMessage(language[11], true);
			try{save.close();}
			catch(IllegalStateException ise){}
		}
	}
}
