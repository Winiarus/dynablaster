
/**
 * Klasa odpowiadająca za protokół komunikacyjny
 * @author Dariusz Błaszkiewicz
 * @author Michał Winiarek
 */

public class Protocol {
	public static final String CHECK = "Are you available";
	public static final String CHECKED = "Server available";
	public static final String SENDINGSCORE = "Sending score";
	public static final String GETHIGHSCORES = "Get high scores";
	public static final String GETCONFIG = "Get config";
	public static final String GETMAP = "Get map";
	public static final String GETLEVEL = "Get level";
	public static final String ERROR = "Error";
}
