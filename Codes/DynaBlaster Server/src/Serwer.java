import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * G��wna klasa serwera
 * 
 * @author Dariusz B�aszkiewicz
 * @author Micha� Winiarek
 */
public class Serwer extends JFrame {
	static Config cfg = Config.getInstance();
	public Serwer() {
		super("Dynablaster");
		Frame frame = new Frame();
		add(frame);
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Serwer server = new Serwer();
				server.setSize(500,500);
				server.setVisible(true);
			}
		});
	}
}
