package game;

import java.awt.Color;

import javax.swing.JLabel;

import elements.Player;

/**
 * klasa odpowiedzialna za odliczanie czasu w grze
 * @author Dariusz B�aszkiewicz
 * @author Micha� Winiarek
 */
public class Time{
	Config cfg = Config.getInstance();
	
	String[] language = cfg.getLanguage(StartFrame.languageIndex);
	public boolean stop;
	public int time;
	public int min;
	public int sec;
	private JLabel label;
	
	public Time(JLabel J){
		time = cfg.time;
		label= J;
		new Thread(new Runnable() {
            public void run() {
                countTime();
            }
        }).start();
	};
	
	/**
	 * synchronizowana metoda liczenia czasu
	 */
	synchronized void countTime(){
		String t;
		
		while(time>=0){			
			if(!Level.pause){				
				min=(int) time/60;
				sec=time%60;
				
				if(sec<10){
					t = (Integer.toString(min)+":0"+Integer.toString(sec));
				}
				else {
					t = (Integer.toString(min)+":"+Integer.toString(sec));
				}
				
				if(time==0){
					label.setText(t);
					label.repaint();
					t = language[8];
					label.setForeground(Color.red);
					GameFrame.getInstance().window.dispose();
					GameOver gameOver = new GameOver();
					
				}								
				label.setText(t);
				label.repaint();
				
				time--;
				
				if(Player.getInstance().resetTime){
					time=-1;
					Player.getInstance().setResetTime();
				}
			}

			try{
				Thread.sleep(1000);
			}
			catch (InterruptedException ie) {ie.printStackTrace();}
		}
	}
}