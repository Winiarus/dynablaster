package game;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Vector;

import elements.BlueSpirit;
import elements.Box;
import elements.Road;
import elements.Spirit;
import elements.Wall;

/**Klasa odpowiedzialna za wczytywanie z plik�w konfiguracyjnych
 * 
 * @author Dariusz B�aszkiewicz
 * @author Micha� Winiarek
 */
public final class Config {
	
	private final static Config instance = new Config();
	public static Config getInstance(){return instance;}
	
	public int level = 1;
	public String ip;
	public int time;
	public int timeout;
	public int port;
	public String[] languagesList;
	public String[] language;
	
	
	private int defaultTime = 10;
	private int defaultPort = 6000;
	private int defaultSocketTimeout = 3000;
	
	private int numberOfBombs, rangeBomb;
	private int pointsBox, pointsOrange, pointsGreen, pointsBlue;
	private int numberOfMaps;
	private int numberOfLevel, widthOfLevel, heightOfLevel;
	private int numberOfBlueSpirit, numberOfOrangeSpirit, numberOfGreenSpirit;
	private Vector<Spirit> spirits = new Vector<>();	
		
	public String getIP(){return ip;}
	public int getPort(){return port;}
	public int getNumberOfBombs(){return numberOfBombs;}
	public int getRangeBomb(){return rangeBomb;}
	public int getPointsBox(){return pointsBox;}
	public int getPointsOrange(){return pointsOrange;}
	public int getPointsGreen(){return pointsGreen;}
	public int getPointsBlue(){return pointsBlue;}
	public int getNumberOfMaps(){return numberOfMaps;}
	public int getNumberOfLevel(){return numberOfLevel;}
	public int getWidthOfLevel(){return widthOfLevel;}
	public int getHeightOfLevel(){return heightOfLevel;}
	public int getNumberOfBlueSpirit(){return numberOfBlueSpirit;}
	public int getNumberOfGreenSpirit(){return numberOfGreenSpirit;}
	public int getNumberOfOrangeSpirit(){return numberOfOrangeSpirit;}	
	
	public void setNumberOfBombs(int i){numberOfBombs = i;}
	public void setTime(int i){time = i;}
	public void setRangeBomb(int i){rangeBomb = i;}
	public void setPointsBox(int i){pointsBox = i;}
	public void setPointsOrange(int i){pointsOrange = i;}
	public void setPointsGreen(int i){pointsGreen = i;}
	public void setPointsBlue(int i){pointsBlue = i;}
	public void setNumberOfMaps(int i){numberOfMaps = i;}
	public void setNumberOfLevel(int i){numberOfLevel = i;}
	public void setWidthOfLevel(int i){widthOfLevel = i;}
	public void setHeightOfLevel(int i){heightOfLevel = i;}
	public void setNumberOfBlueSpirit(int i){numberOfBlueSpirit = i;}
	public void setNumberOfGreenSpirit(int i){numberOfGreenSpirit = i;}
	public void setNumberOfOrangeSpirit(int i){numberOfOrangeSpirit = i;}	
	
	private String[] defaultLanguagesList = {"Default"};
	private String[] defaultLanguage = {"Start", "Exit", "HighScore", "Help", 
		"Select Language", "Time", "Lives", "Score", 
		"Time Out", "Check", "Server IP", "Port", 
		"Server Status", "ERROR", "CHECKED", "Wait", "Set My Name", "OK"
		};
		
	private Config(){
		timeout = getValue("#SocketTimeout", defaultSocketTimeout);
		time = getValue("#Time", defaultTime);
		port = getValue("#Port", defaultPort);
		ip = fromFile("#IP");
		languagesList = getLanguagesList();
		if(!StartFrame.online){
			numberOfBombs = getValue("#NumberOfBombs", 2);
			rangeBomb = getValue("#RangeBomb", 2);
			pointsBox = getValue("#PointsBox", 10);
			pointsOrange = getValue("#PointsOrange", 100);
			pointsGreen = getValue("#PointsGreen", 125);
			pointsBlue = getValue("#PointsBlue", 150);
			numberOfMaps = getValue("#NumberOfMaps", 2);
			updateLevel();
		}
	}
	
	/**
	 * metoda zmieniaj�ca parametry nowego poziomu
	 */
	public void updateLevel(){
		heightOfLevel = getValue("#Lvl"+level+"Height", 0);
		widthOfLevel = getValue("#Lvl"+level+"Width", 0);
		numberOfBlueSpirit = getValue("#Lvl"+level+"NumberOfBlueSpirit", 0);
		numberOfOrangeSpirit = getValue("#Lvl"+level+"NumberOfOrangeSpirit", 0);
		numberOfGreenSpirit = getValue("#Lvl"+level+"NumberOfGreenSpirit", 0);
	}

	/**
	 * metoda ustawiajaca parametry polaczenia
	 * @param tmpIp
	 * @param tmpPort
	 */
	public void setConfig(String tmpIp, int tmpPort){
		ip=tmpIp;
		port=tmpPort;
	}
	
	/**Zamiana stringa z pliku na int
	 * 
	 * @param key klucz dla porz�danej liczby
	 * @param defaultValue warto��, przy nieudanym wczytaniu pliku
	 * 
	 * @return liczba dla danego klucza
	 */
	public int getValue(String key, int defaultValue){
		
		String tmp = fromFile(key);
		int tmpValue;
		if(tmp == null){
			return defaultValue;
		}
		try{
			tmpValue = Integer.parseInt(tmp);
		} catch (NumberFormatException e){
			return defaultValue;
		}
		return tmpValue;
	}
	
	/**Pobieranie listy j�zyk�w
	 * 
	 * @return tablica j�zyk�w	  
	 */
	private String[] getLanguagesList(){
		File file = new File("Languages/languagesList.txt");
		Scanner in = null;
		try{
			in = new Scanner(file);
		} catch(FileNotFoundException e){
			return defaultLanguagesList;
		}
		
		String[] tmp = new String[99];
		int i = 1;
		tmp[0] = "Default";
		try{
			in.nextLine();
			while (true) {
				tmp[i] = in.nextLine();
				i++;
			}
		}catch(NoSuchElementException e){
			String[] lang = new String[i];
			for (int j = 0; j < i; j++)
				lang[j] = tmp[j];
			try {
				in.close();
			} catch (IllegalStateException ise) {
			}
			return lang;
		}catch (IllegalStateException e) {
			try {
				in.close();
			} catch (IllegalStateException ise) {
			}
			return defaultLanguagesList;
		}
	}
	
	/**
	 * metoda pobieraj�ca najlepsze wyniki z dysku twardego
	 * @return
	 */
	public String[] getHighScoreOffline(){
		File file = new File("Scores/HighScores.txt");
		Scanner in = null;
		try{
			in = new Scanner(file);
		} catch(FileNotFoundException e){
			return defaultLanguagesList;
		}
		
		String[] tmp = new String[99];
		int i = 0;
		try{
			while (true) {
				tmp[i] = in.nextLine();
				++i;
			}
		}catch(NoSuchElementException e){
			String[] lang = new String[i];
			for (int j = 0; j < i; j++)
				lang[j] = tmp[j];
			try {
				in.close();
			} catch (IllegalStateException ise) {
			}
			return lang;
		}catch (IllegalStateException e) {
			try {
				in.close();
			} catch (IllegalStateException ise) {
			}
			return defaultLanguagesList;
		}
	}

	/**Pobieranie oczekiwanej warto�ci z pliku konfiguracyjnego
	 * 
	 * @param key klucz dla porz�danego tekstu
	 * @return porz�dany tekst
	 */
	private String fromFile(String key){
		File file = new File("config.txt");
		Scanner in = null;
		try{
			in = new Scanner(file);
		} catch (FileNotFoundException e){
			return null;
		}
		String value = null;
		try{
			while(true){
				value = in.nextLine();
				if(value.equals(key)){
					value = in.nextLine();
					break;
				}
			}
		} catch(NoSuchElementException e){
			try{
				in.close();
			} catch(IllegalStateException ise){	
			}
			return null;
		} catch (IllegalStateException e) {
			try {
				in.close();
			} catch (IllegalStateException ise) {
			}
			return null;
		} 
		try{
			in.close();
		} catch(IllegalStateException ise){	
		}
		return value;
	}
	
	/** Wczytywanie mapy z pliku
	 * 
	 * @param lvl poziom gry
	 * @param height ilo�� obiekt�w mapy w pionie 
	 * @param width ilo�� obiekt�w mapy w poziomie 
	 */
	public void mapFromFile(int lvl, int height, int width){
		
		File file = new File("config.txt");
		String key = "#Lvl"+lvl+"Map";
		int map[][] = new int[width][height];
		Scanner in = null;
		try{
			in = new Scanner(file);
		} catch (FileNotFoundException e){
		}
		String value = null;
		
		try{
			while(true){
				value = in.nextLine();
				if(value.equals(key)){
					for(int h=0; h<height; h++){
						String line = in.nextLine();
						 for(int w = 0; w<width; w++){	
							map[w][h] = Character.getNumericValue(line.charAt(w));
							GameBoard.getInstance();
							if (map[w][h] == 0){
								GameBoard.setElement(w, h, new Road());
							}
							else if (map[w][h] == 1){
								GameBoard.setElement(w, h, new Wall());
							}
							else if (map[w][h] == 2){
								GameBoard.setElement(w, h, new Box());
							}
						}
					}
					break;
				}
			}
		} catch(NoSuchElementException e){
			try{
				in.close();
			} catch(IllegalStateException ise){	
			}
		} catch (IllegalStateException e) {
			try {
				in.close();
			} catch (IllegalStateException ise) {
			}
		} 
		try{
			in.close();
		} catch(IllegalStateException ise){	
		}
	}
	
	/**Wczytywanie j�zyka z pliku
	 * 
	 * @param index numer odpowiadaj�cy danemu j�zykowi
	 * @return wyrazy w danym j�zyku
	 */
	private String[] getLanguageFromFile(int index){
		if(index == 0)
			return defaultLanguage;
		File file = new File("Languages/" + languagesList[index] + ".txt");
		Scanner in = null;
		int count = defaultLanguage.length;
		try{
			in = new Scanner(file);
		}catch(FileNotFoundException e){
			return defaultLanguage;
		}
		String[] tmp = new String[count];
		try{
			for(int i=0; i<count; i++){
				tmp[i] = in.nextLine();
			}
		}catch (NoSuchElementException e) {
			try {
				in.close();
			} catch (IllegalStateException ise) {
			}
			return defaultLanguage;
		} catch (IllegalStateException e) {
			try {
				in.close();
			} catch (IllegalStateException ise) {
			}
			return defaultLanguage;
		}
		try {
			in.close();
		} catch (IllegalStateException ise) {
		}
		return tmp;
	}
	
	/**Metoda zwracaj�ca j�zyk 
	 * 
	 * @param index numer odpowiadaj�cy danemu j�zykowi 
	 * @return wyrazy w danym j�zyku
	 */
	public String[] getLanguage(int index){
		return getLanguageFromFile(index);
	}
	
	/**Metoda zwracaj�ca nazw� j�zyka
	 * 
	 * @param index numer odpowiadaj�cy danemu j�zykowi 
	 * @return nazwa j�zyka
	 */
	public String getLanguagesNames(int index){
		return languagesList[index];
	}
	
	/**Metoda zwracaj�ca tablic� dost�pnych j�zyk�w
	 * 
	 * @return tablica j�zyk�w
	 */
	public String[] getLanguagesNames(){
		return getLanguagesList();
	}
	
	/**Metoda zwracaj�ca rekord dla podanego klucza
	 * 
	 * @param key klucz
	 * @return rekord
	 */
	public String getFromFile(String key){
		return fromFile(key);
	}
	
	/**Funkcja zwracaj�ca liczb� dost�pnych j�zyk�w
	 * 
	 * @return liczba j�zyk�w
	 */
	public int getLangugagesNumber(){
		return getLanguagesList().length;
	}
	
	/**
	 * metoda pobierajaca wektor duszkow
	 * @return
	 */
	public Vector<Spirit> getSpirits(){
		return spirits;
	}
	
	/**
	 * metoda pobierajaca danego duszka
	 * @param i
	 * @return
	 */
	public Spirit getSpirit(int i){
		return spirits.get(i);
	}
	
	/**
	 * metoda dodajaca duszka
	 * @param s
	 */
	public void setSpirit(Spirit s){
		spirits.add(s);
	}
	
	/**
	 * metoda usuwajaca duszki
	 */
	public void deleteSpirits(){
		spirits.removeAllElements();
	}	
}
