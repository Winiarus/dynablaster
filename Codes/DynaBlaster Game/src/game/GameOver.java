package game;

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.NoSuchElementException;
import java.util.Scanner;

import javax.swing.JOptionPane;

import elements.Player;

/**
 * klasa odpowiedzialna za zakonczenie gry i podanie listy najlepszych wynik�w
 * @author Dariusz B�aszkiewicz
 * @author Micha� Winiarek
 */
public class GameOver {
	
	Config cfg = Config.getInstance();
	Player player = Player.getInstance();
	
	public String[] language;
	public String[] highscore;
	
	String name;
	String score;
	String port;
	
	/**
	 * konstruktor bezparametrowy
	 */
	public GameOver(){
		language = cfg.getLanguage(StartFrame.languageIndex);
        name = JOptionPane.showInputDialog(language[16]);
        port = Integer.toString(cfg.port);
        
        score = Integer.toString(player.getScore());
        saveScore(name, score);
        
        if(StartFrame.online==true){
        	Client.sendScore(name, score);
        	try {
				JOptionPane.showMessageDialog(null, Client.getHighScores(cfg.ip, port));
			} catch (HeadlessException | NumberFormatException | IOException e) {
				e.printStackTrace();
			}
        }
        else{
        	highscore = cfg.getHighScoreOffline();
        	JOptionPane.showMessageDialog(null, (highscore));
        }
        System.exit(0);
	}
	
	/**
	 * metoda zapisujaca wynik do pliku z wynikami
	 * @param name
	 * @param score
	 */
	public void saveScore(String name, String score){
		File file = new File("Scores/HighScores.txt");
		Scanner in = null;
		try{
			in = new Scanner(file);
		}
		catch(FileNotFoundException e){}
		
		String[] names = new String[10];
		String[] scores = new String[10];
		
		try{
			for(int i=0; i<10; i++){
				names[i] = in.nextLine();
				scores[i] = in.nextLine();
				}
			}
		catch(NoSuchElementException e){
			try{ in.close();}
			catch(IllegalStateException ise){}
			return;
			}
		catch(IllegalStateException e){
			try{ in.close();}
			catch(IllegalStateException ise){}
			return;
		}
		try{ in.close();}
		catch(IllegalStateException e){}
		try{
			int high = Integer.parseInt(score);
			for(int i=0; i<10; i++){
				if(Integer.parseInt(scores[i])<high){
					for(int j =9; j>i; j--){
						scores[j] = scores[j-1];
						names[j] = names[j-1];
					}
					scores[i] = score;
					names[i] = name;
					break;
				}
				if(i==9){return;}
			}
		}
		catch(NumberFormatException e){}
		PrintWriter save = null;
		try{
			save = new PrintWriter("Scores/HighScores.txt");
			for(int i=0; i<10; i++){
				save.println(names[i]);
				save.println(scores[i]);
			}	
			save.close();
		}
		catch(FileNotFoundException e){
		}
		catch(IllegalStateException e ){
			try{save.close();}
			catch(IllegalStateException ise){}
		}
	}
}
