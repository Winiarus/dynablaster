package game;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.Random;
import java.util.Vector;

import javax.swing.JPanel;

import elements.BlueSpirit;
import elements.Bomb;
import elements.Box;
import elements.Fire;
import elements.GreenSpirit;
import elements.OrangeSpirit;
import elements.Player;
import elements.Spirit;

/**
 * Klasa reprezentująca poziom
 * @author Dariusz Błaszkiewicz
 * @author Michał Winiarek
 */
public class Level extends JPanel implements ComponentListener, KeyListener, Runnable, ImageObserver{
	
	Config cfg = Config.getInstance();
	Player player = Player.getInstance();
	GameBoard gb = GameBoard.getInstance();
	
	public boolean boolFire = true;
	public static boolean pause;
	private BufferedImage map;
	private int level;
	private int height, width;
	private int x, y;
	private int w_, h_;
	public static int elementWidth, elementHeight;
	private int windowWidth, windowHeight;
	
	private int numberOfSpirits;
	private int numberOfBlueSpirits;
	private int numberOfOrangeSpirits;
	private int numberOfGreenSpirits;
	
	private int portalX, portalY;
	private int bonusRangeX, bonusRangeY;
	private int bonusBombsX, bonusBombsY;
	
	private static Vector<Bomb> bombs = new Vector<>();
	private static int numberOfBombs;
	private static int rangeBomb;
	
	private Vector<Fire> fires = new Vector<>();
	private static Vector<Thread> ThreadSpirit= new Vector<>();
			
	/** 
	 * konstruktor parametrowy
	 * @param lvl
	 */
	public Level(int lvl){
		
		level = lvl;
		height = cfg.getHeightOfLevel();
		width = cfg.getWidthOfLevel();
		
		numberOfBlueSpirits = cfg.getNumberOfBlueSpirit();
		numberOfOrangeSpirits = cfg.getNumberOfOrangeSpirit();
		numberOfGreenSpirits = cfg.getNumberOfGreenSpirit();
		numberOfSpirits = numberOfBlueSpirits+numberOfGreenSpirits+numberOfOrangeSpirits;	
		
		numberOfBombs = cfg.getNumberOfBombs();
		rangeBomb = cfg.getRangeBomb();
		
		if(!StartFrame.online){
			cfg.mapFromFile(level, height, width);
			int actualSpirit = 0;
			for(int i=0; i<numberOfBlueSpirits ;i++){
				cfg.setSpirit(new BlueSpirit(cfg.getValue("#Lvl"+level+"BlueSpirit"+i+"X", 0), cfg.getValue("#Lvl"+level+"BlueSpirit"+i+"Y", 0), actualSpirit));
				actualSpirit++;
			}
			for(int i=0; i<numberOfOrangeSpirits ;i++){
				cfg.setSpirit(new OrangeSpirit(cfg.getValue("#Lvl"+level+"OrangeSpirit"+i+"X", 0), cfg.getValue("#Lvl"+level+"OrangeSpirit"+i+"Y", 0), actualSpirit));
				actualSpirit++;
			}
			for(int i=0; i<numberOfGreenSpirits ;i++){
				cfg.setSpirit(new GreenSpirit(cfg.getValue("#Lvl"+level+"GreenSpirit"+i+"X", 0), cfg.getValue("#Lvl"+level+"GreenSpirit"+i+"Y", 0), actualSpirit));
				actualSpirit++;
			}
		}			
		
		for(int i=0; i<getNumberOfSpirits(); i++){
			cfg.getSpirit(i).setStepX(elementWidth*2);
			cfg.getSpirit(i).setStepY(elementHeight*2);
			ThreadSpirit.add(new Thread(cfg.getSpirit(i)));
		}
		
		for(Thread t: ThreadSpirit){
			t.start();
		}
		
		setCoordinatesOfAdds();
		addComponentListener(this);
	}
	
	public static Vector<Bomb> getBombs(){return bombs;}
	public Vector<Fire> getFire(){return fires;}
	public int getNumberOfSpirits(){return numberOfSpirits;}
	public static int getRangeBomb(){return rangeBomb;}
	public void setRangeBomb(int i){rangeBomb = i;}

	public static void addRangeBomb(int i){rangeBomb = rangeBomb + i;}
	public static void addNumberBomb(int i){numberOfBombs = numberOfBombs + i;}
	
	public static void deleteThreadSpirits(){
		ThreadSpirit.clear();	
	}
		
	public static void deleteThreadSpirit(int i){
		ThreadSpirit.removeElementAt(i);	
	}
	
	/**
	 * metoda ustawiajaca pozycje bonusów i portalu
	 */
	private void setCoordinatesOfAdds(){
		boolean ok = false;
		int numberOfBoxes = 0;
		int intPortal=0, intRange=0, intBombs=0;
		for(int i=0; i<width; i++){
			for(int j=0; j<height; j++){
				if(gb.getElement(i, j).isBox()){
					numberOfBoxes++;										
				}
			}
		}
		
		Random randomPortal = new Random();
		intPortal = randomPortal.nextInt(numberOfBoxes) + 1;
		//intPortal=1;
		
		while(!ok){
			Random randomRange = new Random();
			intRange = randomRange.nextInt(numberOfBoxes) + 1;
			if(intRange != intPortal){
				ok = true;
			}
		}
		ok = false;
		while(!ok){
			Random randomBombs = new Random();
			intBombs = randomBombs.nextInt(numberOfBoxes) + 1;
			if(intBombs != intPortal && intBombs != intRange){
				ok = true;
			}
		}	
		numberOfBoxes = 0;
		for(int i=0; i<width; i++){
			for(int j=0; j<height; j++){
				if(gb.getElement(i, j).isBox()){
					numberOfBoxes++;
					if(numberOfBoxes==intPortal){
						portalX = i;
						portalY = j;
					}
					if(numberOfBoxes==intRange){
						bonusRangeX = i;
						bonusRangeY = j;
					}
					if(numberOfBoxes==intBombs){
						bonusBombsX = i;
						bonusBombsY = j;
					}
				}
			}
		}
 	}
	
	/**
	 * metoda na bierzaco ustawiajaca parametry poziomu 
	 */
	public void setValue(){
		x = getX(); //zwraca x gornego rogu JPanela 
		y = getY(); //zwraca y gornego rogu JPanela
		y = y-50; //trzeba opuscic o wysokosc okna z wynikami
		w_ = getWidth(); //zwraca szerokosc JPanela
		h_ = getHeight(); //zwraca wysokosc JPanela
		elementWidth = (int) (w_/width); //szerokosc elementu
		elementHeight = (int) (h_/height); //wysokosc elementu
		windowWidth = elementWidth*width;
		windowHeight = elementHeight*height;
		player.setPosition(player.getFinishX()*elementWidth, player.getFinishY()*elementHeight);
		player.setStepX(elementWidth*2);
		player.setStepY(elementHeight*2);
		for(int i=0; i<cfg.getSpirits().size(); i++){
			Spirit s = cfg.getSpirit(i);
			s.setPosition(elementWidth*s.getStartX(), elementHeight*s.getStartY());
			s.setStop(elementWidth*s.getFinishX(), elementHeight*s.getFinishY());
			cfg.getSpirit(i).setStepX(elementWidth*2);
			cfg.getSpirit(i).setStepY(elementHeight*2);
		}
		
		createMap(windowWidth, windowHeight);
	}
	
	/**
	 * metoda tworzaca mape
	 * @param ww
	 * @param hh
	 */
	public void createMap(int ww, int hh){
		BufferedImage bg = new BufferedImage(ww, hh, BufferedImage.TYPE_INT_RGB);
		Graphics g = bg.getGraphics();
		for(int h=0; h<height; h++){
			for(int w=0; w<width; w++){
				g.drawImage(GameBoard.getInstance().getElement(w, h).getImage().getScaledInstance(elementWidth, elementHeight, Image.SCALE_SMOOTH), ((w*elementWidth)), ((h*elementHeight)), this);
			}
		}
		map = bg;
	}
	
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawImage(map, 0, 0, this);
		for(int i=0; i<cfg.getSpirits().size(); i++){
			g.drawImage(cfg.getSpirit(i).getIcon().getImage(), cfg.getSpirit(i).getX(), cfg.getSpirit(i).getY(), elementWidth, elementHeight, this);
		}
		
		for(int i=0; i<bombs.size(); i++){
			g.drawImage(bombs.get(i).getIcon().getImage(), bombs.get(i).getRealX(), bombs.get(i).getRealY(), elementWidth, elementHeight, this);
		}
		for(int i=0; i<fires.size(); i++){
			g.drawImage(fires.get(i).getIcon().getImage(), fires.get(i).getRealX(), fires.get(i).getRealY(), elementWidth, elementHeight, this);
		}
		paintPlayer(g);
	}
	
	/**
	 * metoda rysujaca gracza
	 * @param g
	 */
	public void paintPlayer(Graphics g){
		g.drawImage(player.getIcon().getImage(), player.getX(), player.getY(), elementWidth, elementHeight, this);
	}
	
	/**
	 * metoda tworzaca bombe
	 */
	public void createBomb(){
		boolean isPossible = true;
		if(getBombs().size()==1){
			if((getBombs().get(0).getX()==player.getFinishX()) & getBombs().get(0).getY()==player.getFinishY()){
				isPossible = false;
			}
		}
		if(isPossible){
			Bomb bomb = new Bomb();
			bombs.add(bomb);
			
			new Thread(new Runnable(){
				int id=0;
				public void run(){
					while(bomb.boolBomb){
						if(player.clearBombs){
							bombs.removeAllElements();
							bomb.boolBomb = false;
							player.clearBombs();
						}
						if(!pause){
							id++;
							if(id==50)
								makeFire(bomb);
							if(id>=59){
								explosion(bomb);
								createMap(windowWidth, windowHeight);
								bomb.boolBomb = false;
							}
						}
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {}
					}
				}
			}).start();
			bomb.setRealPos(elementWidth*bomb.getX(), elementHeight*bomb.getY());
		}
	}
	
	/**
	 * metoda tworzaca ogien naokoło danej bomby
	 * @param bomb
	 */
	public void makeFire(Bomb bomb){
		int r = getRangeBomb();
		Fire fire = new Fire();
		fire.setPos(bomb.getX(), bomb.getY());
		fire.setRealPos(elementWidth*bomb.getX(), elementHeight*bomb.getY());
		fires.add(fire);
		
		for(int i=0; i<r; i++){
			if(bomb.getY()+i+1>=0 && bomb.getY()+i+1 < height){
				if(gb.getElement(bomb.getX(), bomb.getY()+i+1).isFireable()){
					Fire fire1 = new Fire();
					fire1.setPos(bomb.getX(), bomb.getY()+i+1);
					fire1.setRealPos(elementWidth*fire1.getX(), elementHeight*fire1.getY());
					fires.add(fire1);
				}
				if(!gb.getElement(bomb.getX(), bomb.getY()+i+1).isNextFireable()){
					break;
				}
			}
		}
		for(int i=0; i<r; i++){
			if(bomb.getY()-i-1>=0 && bomb.getY()-i-1 < height){
				if(gb.getElement(bomb.getX(), bomb.getY()-i-1).isFireable()){
					Fire fire1 = new Fire();
					fire1.setPos(bomb.getX(), bomb.getY()-i-1);
					fire1.setRealPos(elementWidth*fire1.getX(), elementHeight*fire1.getY());
					fires.add(fire1);
				}
				if(!gb.getElement(bomb.getX(), bomb.getY()-i-1).isNextFireable()){
					break;
				}
			}
		}
		for(int i=0; i<r; i++){
			if(bomb.getX()-i-1>=0 && bomb.getX()-i-1 < width){
				if(gb.getElement(bomb.getX()-i-1, bomb.getY()).isFireable()){
					Fire fire1 = new Fire();
					fire1.setPos(bomb.getX()-i-1, bomb.getY());
					fire1.setRealPos(elementWidth*fire1.getX(), elementHeight*fire1.getY());
					fires.add(fire1);
				}
				if(!gb.getElement(bomb.getX()-i-1, bomb.getY()).isNextFireable()){
					break;
				}
			}
		}
		for(int i=0; i<r; i++){
			if(bomb.getX()+i+1>=0 && bomb.getX()+i+1 < width){
				if(gb.getElement(bomb.getX()+i+1, bomb.getY()).isFireable()){
					Fire fire1 = new Fire();
					fire1.setPos(bomb.getX()+i+1, bomb.getY());
					fire1.setRealPos(elementWidth*fire1.getX(), elementHeight*fire1.getY());
					fires.add(fire1);
				}
				if(!gb.getElement(bomb.getX()+i+1, bomb.getY()).isNextFireable()){
					break;
				}
			}
		}
		boolFire = true;
		
		new Thread(new Runnable(){
			int idd=0;
			public void run(){
				while(boolFire){
					
					if(!pause){
						idd++;
						
						if(idd==10){
							fires.clear();
							boolFire = false;
						}
					}
					try{
						Thread.sleep(50);
					} catch(InterruptedException ie) {}
				}
			}
		}).start();
	}
	
	/**
	 * metoda odpowiadajaca za wybuch bomby
	 * @param bomb
	 */
	public void explosion(Bomb bomb){
		if(getBombs().size()>0){
			getBombs().removeElementAt(0);
		}
		
		int r = getRangeBomb();
		
		if(bomb.getX()==player.getFinishX() & bomb.getY()==player.getFinishY()){
			player.lostLife();
		}		
		
		for(int i=0; i<r; i++){
			if(bomb.getY()+i+1>=0 && bomb.getY()+i+1 < height){				
				if(bomb.getX()==player.getFinishX() && bomb.getY()+i+1==player.getFinishY()){
					player.lostLife();
				}
				if(gb.getElement(bomb.getX(), bomb.getY()+i+1).isBox()){
					if(bomb.getX() == portalX & bomb.getY()+i+1 == portalY){
						gb.getElement(bomb.getX(), bomb.getY()+i+1).explosionToPortal(bomb.getX(), bomb.getY()+i+1);
						break;
					}
					else if(bomb.getX() == bonusBombsX & bomb.getY()+i+1 == bonusBombsY){
						gb.getElement(bomb.getX(), bomb.getY()+i+1).explosionToBonusBombs(bomb.getX(), bomb.getY()+i+1);
						break;
					}
					else if(bomb.getX() == bonusRangeX & bomb.getY()+i+1 == bonusRangeY){
						gb.getElement(bomb.getX(), bomb.getY()+i+1).explosionToBonusRange(bomb.getX(), bomb.getY()+i+1);
						break;
					}
					else{
						gb.getElement(bomb.getX(), bomb.getY()+i+1).explosion(bomb.getX(), bomb.getY()+i+1);
						break;
					}
				} else{
					for(int j=0; j<cfg.getSpirits().size(); j++){
						if((cfg.getSpirit(j).getFinishX()==bomb.getX()) && (cfg.getSpirit(j).getFinishY()==bomb.getY()+i+1)){
							player.updateScore(cfg.getSpirit(j).getScore());
							deleteThreadSpirit(j);
							cfg.getSpirit(j).existing = false;
							cfg.getSpirits().remove(j);
						}
					}
				}
				if(!gb.getElement(bomb.getX(), bomb.getY()+i+1).isNextFireable()){
					break;
				}
			}
		}
		for(int i=0; i<r; i++){
			if(bomb.getY()-i-1>=0 && bomb.getY()-i-1 < height){
				if(bomb.getY()-i-1==player.getFinishY() && bomb.getX()==player.getFinishX()){
					player.lostLife();
				}				
				if(gb.getElement(bomb.getX(), bomb.getY()-i-1).isBox()){
					if(bomb.getX() == portalX && bomb.getY()-i-1 == portalY){
						gb.getElement(bomb.getX(), bomb.getY()-i-1).explosionToPortal(bomb.getX(), bomb.getY()-i-1);
						break;
					}
					else if(bomb.getX() == bonusBombsX && bomb.getY()-i-1 == bonusBombsY){
						gb.getElement(bomb.getX(), bomb.getY()-i-1).explosionToBonusBombs(bomb.getX(), bomb.getY()-i-1);
						break;
					}
					else if(bomb.getX() == bonusRangeX && bomb.getY()-i-1 == bonusRangeY){
						gb.getElement(bomb.getX(), bomb.getY()-i-1).explosionToBonusRange(bomb.getX(), bomb.getY()-i-1);
						break;
					}
					else{
						gb.getElement(bomb.getX(), bomb.getY()-i-1).explosion(bomb.getX(), bomb.getY()-i-1);
						break;
					}
				} else{
					for(int j=0; j<cfg.getSpirits().size(); j++){
						if((cfg.getSpirit(j).getFinishX()==bomb.getX()) && (cfg.getSpirit(j).getFinishY()==bomb.getY()-i-1)){
							player.updateScore(cfg.getSpirit(j).getScore());
							deleteThreadSpirit(j);
							cfg.getSpirit(j).existing = false;
							cfg.getSpirits().remove(j);
						}
					}
				}
				if(!gb.getElement(bomb.getX(), bomb.getY()-i-1).isNextFireable()){
					break;
				}
			}
		}
		for(int i=0; i<r; i++){
			if(bomb.getX()-i-1>=0 && bomb.getX()-i-1 < width){
				if(bomb.getY()==player.getFinishY() && bomb.getX()-i-1==player.getFinishX()){
					player.lostLife();
				}				
				if(gb.getElement(bomb.getX()-i-1, bomb.getY()).isBox()){
					
					if(bomb.getX()-i-1 == portalX && bomb.getY() == portalY){
						gb.getElement(bomb.getX()-i-1, bomb.getY()).explosionToPortal(bomb.getX()-i-1, bomb.getY());
						break;
					}
					else if(bomb.getX()-i-1 == bonusBombsX && bomb.getY() == bonusBombsY){
						gb.getElement(bomb.getX()-i-1, bomb.getY()).explosionToBonusBombs(bomb.getX()-i-1, bomb.getY());
						break;
					}
					else if(bomb.getX()-i-1 == bonusRangeX && bomb.getY() == bonusRangeY){
						gb.getElement(bomb.getX()-i-1, bomb.getY()).explosionToBonusRange(bomb.getX()-i-1, bomb.getY());
						break;
					}
					else{
						gb.getElement(bomb.getX()-i-1, bomb.getY()).explosion(bomb.getX()-i-1, bomb.getY());
						break;
					}
				} else{
					for(int j=0; j<cfg.getSpirits().size(); j++){
						if((cfg.getSpirit(j).getFinishX()==bomb.getX()-i-1) && (cfg.getSpirit(j).getFinishY()==bomb.getY())){
							player.updateScore(cfg.getSpirit(j).getScore());
							deleteThreadSpirit(j);
							cfg.getSpirit(j).existing = false;
							cfg.getSpirits().remove(j);
						}
					}
				}
				if(!gb.getElement(bomb.getX()-i-1, bomb.getY()).isNextFireable()){
					break;
				}
			}
		}
		for(int i=0; i<r; i++){
			if(bomb.getX()+i+1>=0 && bomb.getX()+i+1 < width){
				if(bomb.getY()==player.getFinishY() && bomb.getX()+i+1==player.getFinishX()){
					player.lostLife();
				}
				if(gb.getElement(bomb.getX()+i+1, bomb.getY()).isBox()){
					if(bomb.getX()+i+1 == portalX && bomb.getY() == portalY){
						gb.getElement(bomb.getX()+i+1, bomb.getY()).explosionToPortal(bomb.getX()+i+1, bomb.getY());
						break;
					}
					else if(bomb.getX()+i+1 == bonusBombsX && bomb.getY() == bonusBombsY){
						gb.getElement(bomb.getX()+i+1, bomb.getY()).explosionToBonusBombs(bomb.getX()+i+1, bomb.getY());
						break;
					}
					else if(bomb.getX()+i+1 == bonusRangeX && bomb.getY() == bonusRangeY){
						gb.getElement(bomb.getX()+i+1, bomb.getY()).explosionToBonusRange(bomb.getX()+i+1, bomb.getY());
						break;
					}
					else{
						gb.getElement(bomb.getX()+i+1, bomb.getY()).explosion(bomb.getX()+i+1, bomb.getY());
						break;
					}
				} else{
					for(int j=0; j<cfg.getSpirits().size(); j++){
						if((cfg.getSpirit(j).getFinishX()==bomb.getX()+i+1) && (cfg.getSpirit(j).getFinishY()==bomb.getY())){
							player.updateScore(cfg.getSpirit(j).getScore());
							deleteThreadSpirit(j);
							cfg.getSpirit(j).existing = false;
							cfg.getSpirits().remove(j);
							
						}
					}
				}
			}
			if(!gb.getElement(bomb.getX()+i+1, bomb.getY()).isNextFireable()){
				break;
			}
		}
	}
		
	@Override
	public void componentHidden(ComponentEvent arg0) {}

	@Override
	public void componentMoved(ComponentEvent arg0) {}

	@Override
	public void componentResized(ComponentEvent arg0) {
		setValue();
	}

	@Override
	public void componentShown(ComponentEvent arg0) {}
	
	@Override
	public void keyPressed(KeyEvent ke) {
		int keyCode = ke.getKeyCode();
		switch (keyCode){
			case KeyEvent.VK_UP:
				if(!pause)
					player.moveUp();
				break;
			case KeyEvent.VK_RIGHT:
				if(!pause)
					player.moveRight();
				break;
			case KeyEvent.VK_LEFT:
				if(!pause)
					player.moveLeft();
				break;
			case KeyEvent.VK_DOWN:
				if(!pause)
					player.moveDown();
				break;
			case KeyEvent.VK_SPACE:
				if(!pause){
					if(getBombs().size()<numberOfBombs){
						createBomb();
					}
				}
				break;
			case KeyEvent.VK_P:
				if(!pause){
					pause = true;
				}else{
					pause = false;
				}
				break;
		}
	}

	@Override
	public void keyReleased(KeyEvent ke) {}

	@Override
	public void keyTyped(KeyEvent ke) {}

	@Override
	public void run() {
		while(true){
			if(player.getToRepaint()){
				createMap(windowWidth, windowHeight);
				player.toRepaintToFalse();
			}
			repaint();
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}