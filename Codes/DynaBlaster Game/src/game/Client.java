package game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import elements.BlueSpirit;
import elements.Box;
import elements.GreenSpirit;
import elements.OrangeSpirit;
import elements.Road;
import elements.Wall;

/**
 * Klasa reprezentująca klienta serwerowego
 * @author Dariusz Błaszkiewicz
 * @author Michał Winiarek
 */
public class Client {
	static Config cfg = Config.getInstance();
	
	/**
	 * metoda pobierajaca najlepsze wyniki
	 * @param ip
	 * @param port
	 * @return
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public static String[] getHighScores(String ip, String port) throws NumberFormatException, IOException{
		String[] answer;
		try{
			answer=connection(Protocol.GETHIGHSCORES, ip, port);
		}
		catch(NumberFormatException e){
			return null;
		}
		catch(IOException e){
			return null;
		}
		return answer;
	}
	
	/**
	 * metoda pobierajaca z serwera ogólne dane konfiguracyjne
	 * @param ip
	 * @param port
	 * @return
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public static String[] getConfig(String ip, String port) throws NumberFormatException, IOException{
		if(StartFrame.online){
			String[] answer;
			try{
				answer=connection(Protocol.GETCONFIG, ip, port);
			}
			catch(NumberFormatException e){
				return null;
			}
			catch(IOException e){
				return null;
			}
			return answer;
		}
		return null;
	}
	
	/**
	 * metoda pobierajaca z serwera dane konfiguracyjne danego poziomu
	 * @param ip
	 * @param port
	 * @return
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public static String[] getMap(String ip, String port) throws NumberFormatException, IOException{
		if(StartFrame.online){
			String[] answer;
			try{
				answer=connection(Protocol.GETMAP, ip, port);
			}
			catch(NumberFormatException e){
				return null;
			}
			catch(IOException e){
				return null;
			}
			return answer;
		}
		return null;
	}
	
	/**
	 * metoda wysyłająca na serwer zdobyty wynik
	 * @param name
	 * @param score
	 */
	public static void sendScore(String name, String score){
		Socket socket;
		try{
			System.out.println(cfg.ip+" "+cfg.port);
			socket = new Socket(cfg.ip, cfg.port);
			socket.setSoTimeout(cfg.timeout);
			OutputStream os = socket.getOutputStream();
			PrintWriter pw = new PrintWriter(os,true);
			pw.println(Protocol.SENDINGSCORE);
			pw.println(name);
			pw.println(score);
		}
		catch(UnknownHostException e){
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Metoda obsługująca połączenie
	 * @param command
	 * @param ip
	 * @param port
	 * @return
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	private static String[] connection(String command, String ip, String port)
			throws NumberFormatException, IOException{
		Socket socket = new Socket(ip, Integer.parseInt(port));
		socket.setSoTimeout(cfg.timeout);
		OutputStream os= socket.getOutputStream();
		PrintWriter pw = new PrintWriter(os,true);
		InputStream is = socket.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		pw.println(command);
		switch(command){
		case Protocol.GETHIGHSCORES: {
			return getLines(20, br, socket);
		}
		case Protocol.GETCONFIG: {
			String [] config = getLines(7,br,socket);
			cfg.setTime(Integer.parseInt(config[0]));
			cfg.setNumberOfBombs(Integer.parseInt(config[1]));
			cfg.setRangeBomb(Integer.parseInt(config[2]));
			cfg.setPointsBox(Integer.parseInt(config[3]));
			cfg.setPointsOrange(Integer.parseInt(config[4]));
			cfg.setPointsGreen(Integer.parseInt(config[5]));
			cfg.setPointsBlue(Integer.parseInt(config[6]));
			return null;
		}
		
		case Protocol.GETMAP:{
			pw.println(cfg.level);
			String numberOfInfo = br.readLine();
			String [] level = getLines(Integer.parseInt(numberOfInfo), br, socket);
			cfg.setWidthOfLevel(Integer.parseInt(level[0]));
			cfg.setHeightOfLevel(Integer.parseInt(level[1]));
			cfg.setNumberOfBlueSpirit(Integer.parseInt(level[2]));
			int id = 0;
			for(int i=0; i<cfg.getNumberOfBlueSpirit() ;i++){
				cfg.setSpirit(new BlueSpirit(Integer.parseInt(level[2+2*i+1]), Integer.parseInt(level[2+2*i+2]), id));
				id++;
			}
			cfg.setNumberOfOrangeSpirit(Integer.parseInt(level[3+2*cfg.getNumberOfBlueSpirit()]));
			for(int i=0; i<cfg.getNumberOfOrangeSpirit() ;i++){
				cfg.setSpirit(new OrangeSpirit(Integer.parseInt(level[3+2*cfg.getNumberOfBlueSpirit()+2*i+1]), Integer.parseInt(level[3+2*cfg.getNumberOfBlueSpirit()+2*i+2]), id));
				id++;
			}
			cfg.setNumberOfGreenSpirit(Integer.parseInt(level[4+2*(cfg.getNumberOfBlueSpirit()+cfg.getNumberOfOrangeSpirit())]));
			for(int i=0; i<cfg.getNumberOfGreenSpirit() ;i++){
				cfg.setSpirit(new GreenSpirit(Integer.parseInt(level[4+2*(cfg.getNumberOfBlueSpirit()+cfg.getNumberOfOrangeSpirit())+2*i+1]), Integer.parseInt(level[4+2*(cfg.getNumberOfBlueSpirit()+cfg.getNumberOfOrangeSpirit())+2*i+2]), id));
				id++;
			}
			int map[][] = new int[cfg.getWidthOfLevel()][cfg.getHeightOfLevel()];
			for(int h=0; h<cfg.getHeightOfLevel(); h++){
				String line = level[5+h+2*(cfg.getNumberOfBlueSpirit()+cfg.getNumberOfOrangeSpirit()+cfg.getNumberOfGreenSpirit())];
				for(int w=0; w<cfg.getWidthOfLevel(); w++){
					map[w][h] = Character.getNumericValue(line.charAt(w));
					GameBoard.getInstance();
					if (map[w][h] == 0){
						GameBoard.setElement(w, h, new Road());
					}
					else if (map[w][h] == 1){
						GameBoard.setElement(w, h, new Wall());
					}
					else if (map[w][h] == 2){
						GameBoard.setElement(w, h, new Box());
					}
				}
			}
			return null;
		}
		
		default: {
			socket.close();
			throw new IOException("Unknown command");
		}
		}
	}
	
	/**
	 * metoda zczytująca dane z serwera do tablicy stringów
	 * @param count
	 * @param br
	 * @param socket
	 * @return
	 * @throws IOException
	 */
	private static String[] getLines(int count, BufferedReader br, Socket socket)
			throws IOException{
		String[] answer = new String[count];
		for(int i=0; i<count; i++){
			answer[i] = br.readLine();
			if(answer[i].equals(Protocol.ERROR))
				throw new IOException("Server Error");
		}
		socket.close();
		return answer;
	}
}
