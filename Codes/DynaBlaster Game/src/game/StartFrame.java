package game;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import elements.Player;

/**
 * Klasa odpowiadaj�ca za okno startowe
 * 
 * @author Dariusz B�aszkiewicz
 * @author Micha� Winiarek
 */
public class StartFrame extends JFrame implements ActionListener {
	
	static Config cfg = Config.getInstance();
	private String[] language;
	private static String[] languages;
	static int languageIndex;
	public static boolean online = false;
	
	public StartFrame(){
		super("DynaBlasterLauncher");
				
		language = cfg.getLanguage(0);
		languageIndex = 0;

		setSize(300, 350);
		setResizable(false);
		
		languages = new String[cfg.getLangugagesNumber()];
		for(int i=0; i<cfg.getLangugagesNumber(); i++){
			languages[i] = cfg.getLanguagesNames(i);
		}
		
		JComboBox languagesList = new JComboBox(languages);		
		languagesList.setSelectedIndex(0);
		languagesList.addActionListener(this);
		languagesList.setPreferredSize(new Dimension(80, 40));   //nie dziala zmniejszanie
		
		JPanel N = new JPanel();
		JPanel S = new JPanel();
		JPanel E = new JPanel();
		JPanel W = new JPanel();
		JPanel C = new JPanel();
		JPanel CS = new JPanel();
		JPanel CN = new JPanel();
		
		N.setBorder(new EmptyBorder(20, 0, 0, 0));
		S.setBorder(new EmptyBorder(0, 0, 20, 0));
		E.setBorder(new EmptyBorder(0, 0, 0, 20));
		W.setBorder(new EmptyBorder(0, 20, 0, 0));
		
		JPanel all = new JPanel();
		JPanel buttons = new JPanel();
		JPanel networks = new JPanel();
		
		all.setBackground(Color.black);
		all.setLayout(new BorderLayout());
		C.setLayout(new BorderLayout());
		CS.setLayout(new BorderLayout());
		
		CN.setLayout(new GridLayout(1, 2, 0, 20));
		
		buttons.setLayout(new GridLayout(3, 1, 0, 20));
		networks.setLayout(new GridLayout(5, 1, 0, 20));
		
		final JLabel iptext = new JLabel(language[10] + ": ");
		iptext.setFont(new Font("Serif", Font.BOLD, 16));
		
		final JLabel porttext = new JLabel(language[11] + ": ");
		porttext.setFont(new Font("Serif", Font.BOLD, 16));
		
		final JLabel selectLanguage = new JLabel(language[4] + ":   ");
		selectLanguage.setFont(new Font("Serif", Font.BOLD, 12));
		selectLanguage.setBackground(Color.green); // T�o dla listy selectLangugage
		
		final JButton start = new JButton(language[0]);
		start.setPreferredSize(new Dimension(100, 25));
		start.setFocusable(false);
		
		final JButton check = new JButton(language[9]);
		check.setPreferredSize(new Dimension(100, 25));
		check.setFocusable(false);
		
		final JLabel serverStatusText = new JLabel(language[12] + ": ");
		serverStatusText.setFont(new Font("Serif", Font.BOLD, 12));
		
		final JButton exit = new JButton(language[1]);
		exit.setPreferredSize(new Dimension(100, 25));
		exit.setFocusable(false);
		
		String tmpIp = cfg.getFromFile("#IP");
		final JTextField ip = new JTextField(tmpIp);
		
		
		String tmpPort = cfg.getFromFile("#Port");
		final JTextField port = new JTextField(tmpPort);
		port.setPreferredSize(new Dimension(85, 25));
	
		buttons.add(start);
		buttons.add(check);
		buttons.add(exit);
		
		networks.add(iptext);
		networks.add(ip);
		networks.add(porttext);
		networks.add(port);
		networks.add(serverStatusText);
		
		CS.add(buttons, BorderLayout.WEST);  //CS.add( ) - dodaj rzeczy zwi�zane z serwerem
		CS.add(networks, BorderLayout.EAST);
		CN.add(selectLanguage);
		CN.add(languagesList);
		
		C.add(CS, BorderLayout.SOUTH);
		C.add(CN, BorderLayout.NORTH);  //dodajemy do NORTH, �eby m�c zmienia� rozmiary kom�rek 
        
		all.add(N, BorderLayout.NORTH);
		all.add(S, BorderLayout.SOUTH);
		all.add(E, BorderLayout.EAST);
		all.add(W, BorderLayout.WEST);
		all.add(C, BorderLayout.CENTER);

		add(all);
		
		setVisible(true);
		
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					
				JFrame game =  GameFrame.getInstance().window;
				try {
					Client.getMap(cfg.ip, Integer.toString(cfg.port));
				} catch (NumberFormatException | IOException e1) {
					e1.printStackTrace();
				}
				GameFrame.getInstance().visible();

				Dimension dm = game.getToolkit().getScreenSize();
				game.setLocation(
					(int) (dm.getWidth() / 2 - game.getWidth() / 2),
					(int) (dm.getHeight() / 2 - game.getHeight() / 2));
				game.setVisible(true);
				new Thread(Player.getInstance()).start();
				setVisible(false);
			}
		});
		
		languagesList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				languageIndex = languagesList.getSelectedIndex();
				language = cfg.getLanguage(languageIndex);
				
				start.setText(language[0]);
				exit.setText(language[1]);
				selectLanguage.setText(language[4] + ":   ");
				check.setText(language[9]);
				porttext.setText(language[11] + ": ");
				iptext.setText(language[10] + ": ");
				serverStatusText.setText(language[12]+": ");
			}
		});
		
		check.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){

				class Check extends Thread{
					
					private String ip_;
					private int port_;
					private String command_;					
					
					public void check(String command, String ip, String port){
						
						serverStatusText.setText(language[15]);
						serverStatusText.setForeground(Color.black);
						serverStatusText.revalidate();
						command_ = command;
						ip_ = ip;
						try{
							port_ = Integer.parseInt(port);
						} catch(NumberFormatException nfe){
							serverStatusText.setText(language[13]);
							serverStatusText.setForeground(Color.green);
							serverStatusText.revalidate();
							return;
						}
						cfg.setConfig(ip_, port_);
						start();
					}
					
					public void run(){
						Socket socket = null;
						try{
							try{
								socket = new Socket(ip_, port_);
								socket.setSoTimeout(cfg.time);
							}catch(IllegalArgumentException iae){
								serverStatusText.setText(language[14]);
								serverStatusText.revalidate();
								return;
							}
							String answer;
							OutputStream os = socket.getOutputStream();
							PrintWriter pw = new PrintWriter(os, true);
							InputStream is = socket.getInputStream();
							BufferedReader br = new BufferedReader(
									new InputStreamReader(is));
							pw.println(command_);
							
							answer = br.readLine();
							socket.close();
							
							if (answer.equals(Protocol.CHECKED)) {
								serverStatusText.setText(language[14]+"!");
								serverStatusText.setForeground(Color.green);
								serverStatusText.revalidate();
								online = true;
								return;
							}
							else{
								serverStatusText.setText(language[13]+"!");
								serverStatusText.setForeground(Color.red);
								serverStatusText.revalidate();
								return;
							}
						}catch(IOException e){
							serverStatusText.setText(language[13]+"!");
							serverStatusText.setForeground(Color.red);
							serverStatusText.revalidate();
							return;
						}
					}
				}
				
				if(!serverStatusText.getText().equals(language[15])){
					Check check = new Check();
					check.check(Protocol.CHECK, ip.getText(), 
							port.getText());
				}
			}
		});  
	}

	public void actionPerformed(ActionEvent e) {
		
	}
}