package game;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.KeyListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import elements.Player;

/**
 * Klasa odpowiadaj�ca za obszar gry
 * @author Dariusz B�aszkiewicz
 * @author Micha� Winiarek
 */
public final class GameFrame {
	
	private final static GameFrame instance = new GameFrame();
	public static GameFrame getInstance(){return instance;}
	
	Config cfg = Config.getInstance();
	GameBoard gb = GameBoard.getInstance();
	static Player player = Player.getInstance();
	
	public JFrame window;
	public int height;
	public int width;
	public static int intLevel;
	
	public String[] language;
	
	private Level level;
	
	private static JPanel frame;
	private static JLabel scoreValue;
	private static JLabel livesValue;
		
	/**
	 * konstruktor bezparametrowy
	 */
	public GameFrame(){
		window = new JFrame("DynaBlaster");
		if(StartFrame.online){
			try {
				Client.getConfig(cfg.ip, Integer.toString(cfg.port));
			} 
			catch (NumberFormatException e) {e.printStackTrace();} 
			catch (IOException e) {e.printStackTrace();	}
		}
	}
	
	/**
	 * metoda ustawiajaca wyglad pola gry
	 */
	public void visible(){
		intLevel = cfg.level;;
		
		language = cfg.getLanguage(StartFrame.languageIndex);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		height = cfg.getHeightOfLevel();
		width = cfg.getWidthOfLevel();
		
		window.setSize(600, 600);
		
		JPanel all = new JPanel();
		frame = new JPanel();
		
		JLabel time = new JLabel(language[5]+":");
		time.setFont(new Font("Serif", Font.BOLD, 22));
		time.setForeground(Color.white);
		time.setHorizontalAlignment(JLabel.CENTER);
		
		JLabel timeValue = new JLabel();
		Time time_ = new Time(timeValue);
		
		timeValue.setFont(new Font("Serif", Font.BOLD, 22));
		timeValue.setForeground(Color.white);
		timeValue.setHorizontalAlignment(JLabel.CENTER);
		
		JLabel lives = new JLabel(language[6]+":");
		lives.setFont(new Font("Serif", Font.BOLD, 22));
		lives.setForeground(Color.white);
		lives.setHorizontalAlignment(JLabel.CENTER);
		
		livesValue = new JLabel(Integer.toString(player.getLives()));
		livesValue.setFont(new Font("Serif", Font.BOLD, 22));
		livesValue.setForeground(Color.white);
		livesValue.setHorizontalAlignment(JLabel.CENTER);
		
		JLabel score = new JLabel(language[7]+":");
		score.setFont(new Font("Serif", Font.BOLD, 22));
		score.setForeground(Color.white);
		score.setHorizontalAlignment(JLabel.CENTER);
		
		scoreValue = new JLabel(Integer.toString(player.getScore()));		
		scoreValue.setFont(new Font("Serif", Font.BOLD, 22));
		scoreValue.setForeground(Color.white);
		scoreValue.setHorizontalAlignment(JLabel.CENTER);
		
	
		frame.setLayout(new GridLayout(2, 1));// zmieni� ilos� pierdol na gornym panelu
		frame.setPreferredSize(new Dimension(600, 50));
		all.setLayout(new BorderLayout());
		
		level = new Level(intLevel);
		
		frame.add(time);
		frame.add(lives);
		frame.add(score);
		frame.add(timeValue);
		frame.add(livesValue);
		frame.add(scoreValue);
			
		frame.setBackground(Color.black);
		level.setBackground(Color.black);
		
		all.add(level, BorderLayout.CENTER);
		all.add(frame, BorderLayout.NORTH);
				
		window.add(all);
		
		window.setVisible(true);
		
		level.setValue();
	
		window.addKeyListener((KeyListener) level);
		new Thread(level).start();
		
	}
	
	/**
	 * metoda uaktualniaj�ca wynik
	 */
	public static void updateScore(){
		scoreValue.setText(Integer.toString(player.getScore()));
		frame.repaint();
	}
	
	/**
	 * metoda uaktualniaj�ca liczbe �y�
	 */
	public static void updateLives(){
		livesValue.setText(Integer.toString(player.getLives()));
		frame.repaint();
	}
}
