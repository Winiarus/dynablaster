package game;

import java.awt.Dimension;

/**
 * G��wna klasa gry
 * 
 * @author Dariusz B�aszkiewicz
 * @author Micha� Winiarek
 */
public class Dynablaster {
	public static void main(String[] args) {
		
		StartFrame Launcher = new StartFrame();
		
		Dimension dm = Launcher.getToolkit().getScreenSize();
		Launcher.setLocation(
				(int) (dm.getWidth() / 2 - Launcher.getWidth() / 2),
				(int) (dm.getHeight() / 2 - Launcher.getHeight() / 2));
		Launcher.setVisible(true);
		
		
	}
}
