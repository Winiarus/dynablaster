package game;

import elements.GameElements;

/**
 * Klasa odpowiedzialna za przechowywanie elementów gry 
 * @author Dariusz Błaszkiewicz
 * @author Michał Winiarek
 */
public final class GameBoard{
	private final static GameBoard instance = new GameBoard();
	public static GameBoard getInstance(){return instance;}
	
	Config cfg = Config.getInstance();
	
	int level = cfg.level;
	static int gameWidth;
	static int gameHeight;
	
	public GameElements[] boxes;
	
	private GameBoard(){
		gameWidth = cfg.getWidthOfLevel();
		gameHeight = cfg.getHeightOfLevel();
	}
	
	public static GameElements[][] elements = new GameElements[gameWidth][gameHeight]; 
	
	/**Funkcja ustawiająca elementy w odpowiednim miejscu tablicy elementów
	 * 
	 * @param w numer kolumny
	 * @param h numer wiersza
	 * @param ge element gry
	 */
	public static void setElement(int w, int h, GameElements ge){
		elements[w][h] = ge;
	}
	
	/**Funkcja pobierająca element z danego miejsca tablicy elementów
	 * 
	 * @param w numer kolumny
	 * @param h numer wiersza
	 * @return element
	 */
	public GameElements getElement(int w, int h){
		return elements[w][h];
	}
}
