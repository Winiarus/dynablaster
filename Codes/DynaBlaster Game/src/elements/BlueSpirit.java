package elements;

import java.io.File;

import javax.swing.ImageIcon;

/**
 * Klasa reprezentująca niebieskiego duszka
 * @author Dariusz Błaszkiewicz
 * @author Michał Winiarek
 */
public class BlueSpirit extends Spirit {
	
	/**
	 * Konstruktor parametrowy niebieskiego duszka
	 * @param x 
	 * @param y
	 * @param id
	 */
	public BlueSpirit (int x, int y, int id){
		super(x, y, id);		
		try{
			imageIcon = new ImageIcon(getClass().getResource("/"+cfg.getFromFile("#ImageBlueSpirit")));
		} catch(Exception e){
			System.err.println("Error");
			e.printStackTrace();
		}
	}
	
	/**
	 * Konstruktor bezparametrowy niebieskiego duszka
	 */
	public BlueSpirit(){
		File file = new File(cfg.getFromFile("#ImageBlueSpirit"));
		try{
			imageIcon = new ImageIcon(file.getPath());
		} catch(Exception e){
			System.err.println("Error");
			e.printStackTrace();
		}
	}
	
	/**
	 * metoda zwracająca liczbe punktow za zabicie niebieskiego duszka
	 */
	public int getScore(){
		return cfg.getPointsBlue();
	}
}