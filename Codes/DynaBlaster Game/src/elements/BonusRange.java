package elements;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

/**
 * Klasa reprezentująca bonus do zasięgu bomb
 * @author Dariusz Błaszkiewicz
 * @author Michał Winiarek
 */
public class BonusRange extends GameElements{
	
	private final static BonusRange instance = new BonusRange();
	public static BonusRange getInstance(){return instance;}
	
	/**
	 * Konstruktor bezparametrowy
	 */
	public BonusRange(){
		InputStream file =getClass().getResourceAsStream("/"+cfg.getFromFile("#ImageBonusRange"));
		try{
			picture = ImageIO.read(file);
		} catch(IOException e){
			System.err.println("Error");
			e.printStackTrace();
		}
	}
	
	public boolean isMovable(){
		return true;
	}
	
	public boolean isFireable(){
		return true;
	}
	
	public boolean isNextFireable(){
		return true;
	}
	
	@Override
	public boolean explosion(int x, int y){return false;}

	@Override
	public boolean explosionToPortal(int x, int y) {return false;}

	@Override
	public boolean explosionToBonusRange(int x, int y) {return false;}

	@Override
	public boolean explosionToBonusBombs(int x, int y) {return false;}
	
	public boolean isBonusRange() {return true;}
	
}
