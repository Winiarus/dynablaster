package elements;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

/**
 * Klasa reprezentująca portal
 * @author Dariusz Błaszkiewicz
 * @author Michał Winiarek
 */
public class Portal extends GameElements{
	
	private final static Portal instance = new Portal();
	public static Portal getInstance(){return instance;}

	public Portal(){
		InputStream file =getClass().getResourceAsStream("/"+cfg.getFromFile("#ImagePortal"));
		try{
			picture = ImageIO.read(file);
		} catch(IOException e){
			System.err.println("Error");
			e.printStackTrace();
		}
	}
	
	public boolean isMovable(){
		return true;
	}
	
	public boolean isFireable(){
		return true;
	}
	
	public boolean isNextFireable(){
		return true;
	}
	
	public boolean isPortal(){
		return true;
	}
	
	@Override
	public boolean explosion(int x, int y){return false;}

	@Override
	public boolean explosionToPortal(int x, int y) {return false;}

	@Override
	public boolean explosionToBonusRange(int x, int y) {return false;}

	@Override
	public boolean explosionToBonusBombs(int x, int y) {return false;}
}
