package elements;

import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import game.Config;
import game.GameBoard;

/**
 * Klasa odpowiadaj�ca za bomb�
 * @author Dariusz B�aszkiewicz
 * @author Micha� Winiarek
 */
public class Bomb extends JLabel{
	Config cfg = Config.getInstance();
	GameBoard gb = GameBoard.getInstance();
	Player player = Player.getInstance();
	
	ImageIcon imageIcon;
	
	private int x, y;
	private int realX, realY;
	public boolean boolBomb = true;
	
	/**
	 * Konstruktor bezparametrowy bomby
	 */
	public Bomb(){
		try{
			imageIcon = new ImageIcon(getClass().getResource("/"+cfg.getFromFile("#ImageBomb")));
		} catch(Exception e){
			System.err.println("Error");
			e.printStackTrace();
		}
		x = player.getFinishX();
		y = player.getFinishY();
	}
	
	/**
	 * Metoda zwracaj�ca obrazek
	 * @return imageIcon
	 */
	public ImageIcon getIcon(){return imageIcon;}
	
	/**
	 * metoda zwracaj�ca pozycje x bomby
	 * @return x
	 */
	public int getX(){return x;}
	
	/**
	 * metoda zwracaj�ca pozycje y bomby
	 * @return y
	 */
	public int getY(){return y;}
	
	/**
	 * metoda zwracaj�ca pozycje x g�nego lewego pixela bomby
	 * @return realX
	 */
	public int getRealX(){return realX;}
	
	/**
	 * metoda zwracaj�ca pozycje y g�nego lewego pixela bomby
	 * @return realY
	 */
	public int getRealY(){return realY;}
	
	/**
	 * Metoda ustawiajaca pozycje bomby na tle mapy
	 * @param x_ 
	 * @param y_
	 */
	public void setPos(int x_, int y_){
		x=x_;
		y=y_;
	}
	
	/**
	 * Metoda ustawiajaca pozycje gornego lewego piksela bomby
	 * @param rx
	 * @param ry
	 */
	public void setRealPos(int rx, int ry){
		realX = rx;
		realY = ry;
	}
}
