package elements;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

/**
 * Klasa reprezentuj�ca drog�
 * @author Dariusz B�aszkiewicz
 * @author Micha� Winiarek
 */
public class Road extends GameElements{
	
	public Road(){
		InputStream file =getClass().getResourceAsStream("/"+cfg.getFromFile("#ImageGrass"));
		try{
			picture = ImageIO.read(file);
		} catch(IOException e){
			System.err.println("Error");
			e.printStackTrace();
		}
	}
	
	public boolean isMovable(){
		return true;
	}
	
	public boolean isFireable(){
		return true;
	}
	
	public boolean isNextFireable(){
		return true;
	}
	
	@Override
	public boolean explosion(int x, int y){return false;}

	@Override
	public boolean explosionToPortal(int x, int y) {return false;}

	@Override
	public boolean explosionToBonusRange(int x, int y) {return false;}

	@Override
	public boolean explosionToBonusBombs(int x, int y) {return false;}
	
	public int getScore(){return 0;}
}
