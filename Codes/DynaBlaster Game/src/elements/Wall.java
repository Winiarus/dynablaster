package elements;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

/**
 * Klasa reprezentująca ścianę
 * @author Dariusz Błaszkiewicz
 * @author Michał Winiarek
 */
public class Wall extends GameElements{
	
	/**
	 * konstruktor bezparametrowy
	 */
	public Wall(){
		InputStream file =getClass().getResourceAsStream("/"+cfg.getFromFile("#ImageWall"));
		try{
			picture = ImageIO.read(file);
		} catch(IOException e){
			System.err.println("Error");
			e.printStackTrace();
		}
	}
	
	public boolean isMovable(){
		return false;
	}
	
	public boolean isFireable(){
		return false;
	}
	
	public boolean isNextFireable(){
		return false;
	}
	
	@Override
	public boolean explosion(int x, int y){return true;}

	@Override
	public boolean explosionToPortal(int x, int y) {return true;}

	@Override
	public boolean explosionToBonusRange(int x, int y) {return true;}

	@Override
	public boolean explosionToBonusBombs(int x, int y) {return true;}
	
	public int getScore(){return 0;}
}
