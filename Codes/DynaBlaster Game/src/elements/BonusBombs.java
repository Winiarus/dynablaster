package elements;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

/**
 * Klasa reprezentująca bonus do liczby bomb
 * @author Dariusz Błaszkiewicz
 * @author Michał Winiarek
 */
public class BonusBombs extends GameElements{
	
	private final static BonusBombs instance = new BonusBombs();
	public static BonusBombs getInstance(){return instance;}
	
	/**
	 * Konstruktor bez parametrowy bonusa do liczby bomb
	 */
	public BonusBombs(){
		InputStream file =getClass().getResourceAsStream("/"+cfg.getFromFile("#ImageBonusBombs"));
		try{
			picture = ImageIO.read(file);
		} catch(IOException e){
			System.err.println("Error");
			e.printStackTrace();
		}
	}
	
	public boolean isMovable(){
		return true;
	}
	
	public boolean isFireable(){
		return true;
	}
	
	public boolean isNextFireable(){
		return true;
	}
	
	@Override
	public boolean explosion(int x, int y){return false;}

	@Override
	public boolean explosionToPortal(int x, int y) {return false;}

	@Override
	public boolean explosionToBonusRange(int x, int y) {return false;}

	@Override
	public boolean explosionToBonusBombs(int x, int y) {return false;}
	
	public boolean isBonusBombs() {return true;}
}
