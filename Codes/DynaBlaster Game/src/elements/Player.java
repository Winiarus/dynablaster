package elements;

import game.Client;
import game.Config;
import game.GameBoard;
import game.GameFrame;
import game.GameOver;
import game.Level;
import game.StartFrame;
import game.Time;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

/**
 * Klasa reprezentuj�ca gracza
 * @author Dariusz B�aszkiewicz
 * @author Micha� Winiarek
 */
public final class Player extends Character{

	private final static Player instance = new Player();
	public static Player getInstance(){return instance;}
	
	GameBoard gb = GameBoard.getInstance();
	Config cfg = Config.getInstance();
	
	public boolean resetTime = false;
	public boolean clearBombs = false;
	
	private int lives;
	private int score;
	public boolean isMoving = false;
	private boolean immortal = false;
	private boolean toRepaint = false;
	
	/**
	 * konstruktor bezparametrowy
	 */
	public Player(){
		lives = 3;
		score = 0;
		stepX = 100;
		stepY = 100;
		x=0;
		y=0;
		finishX = 1;
		finishY = 1;
		startX = 1;
		startY = 1;
		try{
			imageIcon = new ImageIcon(getClass().getResource("/"+cfg.getFromFile("#ImagePlayer")));
		} catch(Exception e){
			System.err.println("Error");
			e.printStackTrace();
		}
	}
	
	public void setPlayerValues(){
		finishX = 1;
		finishY = 1;
		startX = 1;
		startY = 1;
	}
	
	public void setResetTime(){
		resetTime = false;
	}
	
	public void clearBombs(){
		clearBombs = false;
	}
	
	/**
	 * metoda zwracajaca liczbe �y� uzytkownika
	 * @return
	 */
	public int getLives() {return lives;}
	
	public boolean getToRepaint(){
		return toRepaint;
	}
	
	public void toRepaintToFalse(){
		toRepaint = false;
	}
	
	/**
	 * metoda zmniejszajaca liczbe zyc, dajaca niesmiertelnosc po jego straceniu na 3s oraz zamykajaca gr� gdy liczba �y� spadnie do 0
	 */
	public void lostLife() {
		if(immortal == false){
			immortal = true;
			lives = lives - 1;
			if(lives==0){
				GameFrame.getInstance().window.dispose();
				GameOver gameOver = new GameOver();
			}
			
			setRealPosition(1, 1);
			setStop(1, 1);
			setPosition(0, 0);
			
			setPosition(Level.elementWidth, Level.elementHeight);
			GameFrame.updateLives();
			new Thread(new Runnable(){
				public void run(){
					try{
						Thread.sleep(3000);
					} catch(InterruptedException ie) {}
					immortal = false;
				}
			}).start();
		}
	}	
	
	/**
	 * metoda zwracajaca liczbe punktow 
	 */
	public int getScore() {return score;}
	
	/**
	 * metoda odswiezajaca liczbe punktow 
	 * @param i
	 */
	public void updateScore(int i){
		score = score + i;
		GameFrame.updateScore();
	}
	
	/**
	 * metoda zmieniajaca poziom na wyzszy
	 */
	public void changeLevel(){
		
		
		Level.deleteThreadSpirits();
		for(int i=0; i<cfg.getSpirits().size(); i++){
			cfg.getSpirit(i).existing = false;
		}
		cfg.deleteSpirits();
		clearBombs = true;
		resetTime = true;
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	
		cfg.level = cfg.level+1;
		
		if(cfg.level==6){
			GameFrame.getInstance().window.dispose();
			GameOver gameOver = new GameOver();
		}
		
		
		
		setRealPosition(1, 1);
		GameFrame.getInstance().window.dispose();
		
		GameFrame.getInstance().window = new JFrame("DynaBlaster");
		JFrame game = GameFrame.getInstance().window;
		
		if(!StartFrame.online){
			cfg.updateLevel();
		}else{
			try {
				Client.getMap(cfg.getIP(), Integer.toString(cfg.getPort()));
			} catch (NumberFormatException | IOException e) {
				System.out.println("blad ");
				e.printStackTrace();
			}
		}
		
		GameFrame.getInstance().visible();
		
		Dimension dm = game.getToolkit().getScreenSize();
		game.setLocation(
			(int) (dm.getWidth() / 2 - game.getWidth() / 2),
			(int) (dm.getHeight() / 2 - game.getHeight() / 2));
		game.setVisible(true);
	}
	
	@Override
	public boolean moveUp(){
		if(isMovable((finishX), (finishY-1)) & !isMoving){
			isMoving = true;
			direction = Direction.Up;
			finishY=finishY-1;
			if(gb.getElement(finishX, finishY).isPortal()){
				changeLevel();
			}
			if(gb.getElement(finishX, finishY).isBonusBombs()){
				Level.addNumberBomb(1);
				GameBoard.setElement(finishX, finishY, new Road());
				toRepaint = true;
			}
			if(gb.getElement(finishX, finishY).isBonusRange()){
				Level.addRangeBomb(1);
				GameBoard.setElement(finishX, finishY, new Road());
				toRepaint = true;
			}
			return true;
		}
		else{
			return false;
		}
	}
	
	@Override
	public boolean moveRight(){
		if(isMovable((finishX+1), (finishY))  & !isMoving){
			isMoving = true;
			direction = Direction.Right;
			finishX=finishX+1;
			if(gb.getElement(finishX, finishY).isPortal()){
				changeLevel();
			}
			if(gb.getElement(finishX, finishY).isBonusBombs()){
				Level.addNumberBomb(1);
				GameBoard.setElement(finishX, finishY, new Road());
				toRepaint = true;
			}
			if(gb.getElement(finishX, finishY).isBonusRange()){
				Level.addRangeBomb(1);
				GameBoard.setElement(finishX, finishY, new Road());
				toRepaint = true;
			}
			return true;
		}
		else{
			return false;
		}
	}
	
	@Override
	public boolean moveLeft(){
		if(isMovable((finishX-1), (finishY)) & !isMoving){
			isMoving = true;
			direction = Direction.Left;
			finishX=finishX-1;
			if(gb.getElement(finishX, finishY).isPortal()){
				changeLevel();
			}
			if(gb.getElement(finishX, finishY).isBonusBombs()){
				Level.addNumberBomb(1);
				GameBoard.setElement(finishX, finishY, new Road());
				toRepaint = true;
			}
			if(gb.getElement(finishX, finishY).isBonusRange()){
				Level.addRangeBomb(1);
				GameBoard.setElement(finishX, finishY, new Road());
				toRepaint = true;
			}
			return true;
		}
		else{
			return false;
		}
	}
	
	@Override
	public boolean moveDown(){
		if(isMovable(finishX, finishY+1) & !isMoving){
			isMoving = true;
			direction = Direction.Down;
			finishY=finishY+1;
			if(gb.getElement(finishX, finishY).isPortal()){
				changeLevel();
			}
			if(gb.getElement(finishX, finishY).isBonusBombs()){
				Level.addNumberBomb(1);
				GameBoard.setElement(finishX, finishY, new Road());
				toRepaint = true;
			}
			if(gb.getElement(finishX, finishY).isBonusRange()){
				Level.addRangeBomb(1);
				GameBoard.setElement(finishX, finishY, new Road());
				toRepaint = true;
			}
			return true;
		}
		else{
			return false;
		}
	}

	@Override
	public void run() {
		while(true){
			if(isMoving){
				int time = 1200;
				int fx;
				int fy;
				
				fx = Level.elementWidth*finishX;
				fy = Level.elementHeight*finishY;
				
				if(direction.equals(Direction.Up)){
					while(true){
						if(!Level.pause){
							if(y>=fy){
								--y;
							} else{
								y=fy;
								break;
							}
						}
						try{
							Thread.sleep((int)(time/stepY));
						}catch(InterruptedException ie){}						
					}
				} else if(direction.equals(Direction.Down)){
					while(true){
						if(!Level.pause){
							if(y<fy){
								++y;
							} else{
								y=fy;
								break;
							}
						}
						try{
							Thread.sleep((int)(time/stepY));
						}catch(InterruptedException ie){}						
					}
				} else if(direction.equals(Direction.Left)){
					while(true){
						if(!Level.pause){
							if(x>fx){
								--x;
							} else{
								x=fx;
								break;
							}
						}
						try{
							Thread.sleep((int)(time/stepX));
						}catch(InterruptedException ie){}						
					}
				} else if(direction.equals(Direction.Right)){
					while(true){
						if(!Level.pause){
							if(x<=fx){
								++x;
							} else{
								x=fx;
								break;
							}
						}
						try{
							Thread.sleep((int)(time/stepX));
						}catch(InterruptedException ie){}						
					}
					
				}
				isMoving = false;
			}
			else{
				try{
					Thread.sleep(10);
				} catch(InterruptedException ie) {}
			}
		}
	}
}