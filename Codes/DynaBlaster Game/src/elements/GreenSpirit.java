package elements;

import java.io.File;

import javax.swing.ImageIcon;

/**
 * Klasa reprezentująca zielonego duszka
 * @author Dariusz Błaszkiewicz
 * @author Michał Winiarek
 */
public class GreenSpirit extends Spirit {
	
	/**
	 * Konstruktor parametrowy zielonego duszka
	 * @param x
	 * @param y
	 * @param id
	 */
	public GreenSpirit (int x, int y, int id){
		super(x, y, id);
		try{
			imageIcon = new ImageIcon(getClass().getResource("/"+cfg.getFromFile("#ImageGreenSpirit")));
		} catch(Exception e){
			System.err.println("Error");
			e.printStackTrace();
		}
	}
	
	/**
	 * Konstruktor bez parametrowy zielonego duszka
	 */
	public GreenSpirit(){
		File file = new File(cfg.getFromFile("#ImageGreenSpirit"));
		try{
			imageIcon = new ImageIcon(file.getPath());
		} catch(Exception e){
			System.err.println("Error");
			e.printStackTrace();
		}
	}
	
	/**
	 * metoda zwracająca liczbe punktow za zabicie zielonego duszka
	 * @return int
	 */
	public int getScore(){
		return cfg.getPointsGreen();
	}
}