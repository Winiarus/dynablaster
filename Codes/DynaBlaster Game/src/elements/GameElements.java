package elements;

import game.Config;
import game.GameBoard;

import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 * Klasa po kt�rej dziedzicz� wszystkie elementy gry
 * @author Dariusz B�aszkiewicz
 * @author Micha� Winiarek
 */
public abstract class GameElements extends JPanel{
	Config cfg = Config.getInstance();
	GameBoard gb = GameBoard.getInstance();
	Player player = Player.getInstance();
	
	public ImageIcon imageIcon;
	protected BufferedImage picture;
	
	/**
	 * Metoda wczytuj�ca obrazek z pliku
	 * @param filename nazwa pliku
	 * @return obrazek
	 */
	protected Image loadImage(String filename){
		return new ImageIcon(filename).getImage();
	}
	
	public BufferedImage getImage(){
		return picture;
	}
	
	/**
	 * Metoda sprawdzajaca czy mozna ruszyc si� na dany element
	 */
	public boolean isMovable(){
		return false;
	}
	
	/**
	 * metoda sprawdzajaca czy element mozna zniszczyc
	 * @return
	 */
	public boolean isFireable(){
		return true;
	}
	
	/**
	 * metoda sprawdzajaca czy mozna zniszczyc element kolejny
	 * @return
	 */
	public boolean isNextFireable(){
		return false;
	}
	
	/**
	 * Metoda ujawniajaca drog� spod skrzynki
	 * @param x
	 * @param y
	 * @return
	 */
	public abstract boolean explosion(int x, int y);
	
	/**
	 * Metoda ujawniajaca portal spod skrzynki
	 * @param x
	 * @param y
	 * @return
	 */
	public abstract boolean explosionToPortal(int x, int y);
	
	/**
	 * Metoda ujawniajaca bonusRange spod skrzynki
	 * @param x
	 * @param y
	 * @return
	 */
	public abstract boolean explosionToBonusRange(int x, int y);
	
	/**
	 * Metoda ujawniajaca bonusBombs spod skrzynki
	 * @param x
	 * @param y
	 * @return
	 */
	public abstract boolean explosionToBonusBombs(int x, int y);
	
	/**
	 * metoda zwracajaca liczbe punktow za zniszczenie obiektu
	 * @return
	 */
	public int getScore(){return 0;}

	/**
	 * metoda sprawdzajaca czy dany element jest portalem
	 * @return
	 */
	public boolean isPortal() {return false;}
	
	/**
	 * metoda sprawdzajaca czy dany element jest skrzynka
	 * @return
	 */
	public boolean isBox(){return false;}
	
	/**
	 * metoda sprawdzajaca czy dany element jest bonusBombs
	 * @return
	 */
	public boolean isBonusBombs() {return false;}
	
	/**
	 * metoda sprawdzajaca czy dany element jest bonusRange
	 * @return
	 */
	public boolean isBonusRange() {return false;}
}