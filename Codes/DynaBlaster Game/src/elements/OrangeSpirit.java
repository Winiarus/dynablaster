package elements;

import java.io.File;
import java.io.InputStream;

import javax.swing.ImageIcon;

/**
 * Klasa reprezentująca pomarańczowego duszka
 * @author Dariusz Błaszkiewicz
 * @author Michał Winiarek
 */
public class OrangeSpirit extends Spirit {
	
	/**
	 * Konstruktor parametrowy pomarańczowego duszka
	 * @param x
	 * @param y
	 * @param id
	 */
	public OrangeSpirit (int x, int y, int id){
		super(x, y, id);
		try{
			imageIcon = new ImageIcon(getClass().getResource("/"+cfg.getFromFile("#ImageOrangeSpirit")));
		} catch(Exception e){
			System.err.println("Error");
			e.printStackTrace();
		}
	}
	
	/**
	 * Konstruktor bez parametrowy niebieskiego duszka
	 */
	public OrangeSpirit(){
		File file = new File(cfg.getFromFile("#ImageOrangeSpirit"));
		try{
			imageIcon = new ImageIcon(file.getPath());
		} catch(Exception e){
			System.err.println("Error");
			e.printStackTrace();
		}
	}
	
	/**
	 * metoda zwracająca liczbe punktow za zabicie pomaranczowego duszka
	 */
	public int getScore(){
		return cfg.getPointsOrange();
	}
}