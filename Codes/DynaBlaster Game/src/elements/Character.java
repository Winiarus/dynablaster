package elements;

import game.Config;
import game.GameBoard;
import game.Level;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * Klasa po kt�rej dziedzicz� wszystkie poruszaj�ce si� elementy gry
 * @author Dariusz B�aszkiewicz
 * @author Micha� Winiarek
 */
public abstract class Character extends JLabel implements Runnable{

	Config cfg = Config.getInstance();
	GameBoard gb = GameBoard.getInstance();
	
	public ImageIcon imageIcon;
	
	protected int x, y;
	protected int startX, startY;
	protected int stopX, stopY; //DESTINY
	protected int finishX, finishY; //CORD
	protected int stepX, stepY;
	
	protected enum Direction{Up, Down, Left, Right};
	protected Direction direction = Direction.Down;
	
	/**
	 * Konstruktor bezparametrowy
	 */
	public Character(){
		x=0;
		y=0;
	}
	
	/**
	 * Konstruktor parametrowy
	 */
	public Character(int x_, int y_){
		finishX = x_; 
		finishY = y_;
		startX = x_;
		startY = y_;
	}
	
	public int getX(){return x;}
	public int getY(){return y;}
	public int getStartX(){return startX;}
	public int getStartY(){return startY;}
	public int getStopX(){return stopX;}
	public int getStopY(){return stopY;}
	public int getFinishX(){return finishX;}
	public int getFinishY(){return finishY;}
	
	public void setStepX(int i){stepX = i;}
	public void setStepY(int i){stepY = i;}
	public void setPosition(int x_, int y_){
		x=x_;
		y=y_;
	}
	
	public void setRealPosition(int x_, int y_){
		finishX = x_; 
		finishY = y_;
		startX = x_;
		startY = y_;
	}
	
	public void setStop(int x_, int y_){
		stopX=x_;
		stopY=y_;
	}
	
	/**
	 * Metoda sprawdzajaca czy jest mozliwosc ruchu na dan� pozycje
	 * @param x_
	 * @param y_
	 * @return
	 */
	public boolean isMovable(int x_, int y_){
		boolean bool = true;
		
		if(gb.getElement(x_, y_).isMovable()){
			
			for(int i=0; i<Level.getBombs().size(); i++){
				if(Level.getBombs().get(i).getX()==x_ & Level.getBombs().get(i).getY()==y_){
					bool = false;
				}
			}
		}
		else{
			bool = false;
		}
		return bool;
	}
	
	public ImageIcon getIcon(){return imageIcon; }
	
	public boolean moveUp(){
		if(isMovable(finishX, finishY-1)){
			--finishY;
			return true;
		} else{
			return false;
		}
	}
	
	public boolean moveDown(){
		if(isMovable(finishX, finishY+1)){
			++finishY;
			return true;
		} else{
			return false;
		}
	}
	
	public boolean moveRight(){
		if(isMovable(finishX+1, finishY)){
			++finishX;
			return true;
		} else{
			return false;
		}
	}
	
	public boolean moveLeft(){
		if(isMovable(finishX-1, finishY)){
			--finishX;
			return true;
		} else{
			return false;
		}
	}
}
