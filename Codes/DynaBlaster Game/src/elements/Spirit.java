package elements;

import java.util.Random;

import game.Config;
import game.GameBoard;
import game.Level;

/**
 * Klasa po kt�rej dziedzicz� duszki
 * @author Dariusz B�aszkiewicz
 * @author Micha� Winiarek
 */
public class Spirit extends Character{
	
	GameBoard gb = GameBoard.getInstance();
	Config cfg = Config.getInstance();
	Player player = Player.getInstance();
	
	protected int spiritId;
	public boolean existing;
	
	/**
	 * konstruktor bezparametrowy
	 */
	public Spirit(){
		existing = true;
	}
	
	/**
	 * Konstruktor parametrowy duszka
	 * @param x 
	 * @param y 
	 * @param id
	 */
	public Spirit(int x_, int y_, int id_){
		existing = true;
		finishX = x_; 
		finishY = y_;
		startX = x_;
		startY = y_;
		spiritId = id_;
	}
	
	/**
	 * metoda sprawdzajaca czy duszek wszedl na gracza
	 * @return
	 */
	public boolean checkCollision(){
		if(player.getFinishX()==finishX && player.getFinishY()==finishY)
			return true;		
		return false;
	}
	
	/**
	 * metoda zwracaja liczbe punktow
	 * @return
	 */
	public int getScore() {return 0;}
	
	@Override
	public void run() {
		int wait = 2000;
		int dir = 0;
		boolean get = false;
		Random random = new Random();
			
		stopX = Level.elementWidth*finishX;
		stopY = Level.elementHeight*finishY;
		
		while(existing){
			if(!Level.pause){		
				get=false;
				while(!get){
					dir = random.nextInt(4);
					switch(dir){
					case 0:
						if(moveUp()){
							finishY++;
							direction = Direction.Up;
							get=true;
						}
						break;
					case 1:
						if(moveDown()){
							finishY--;
							direction = Direction.Down;
							get=true;
						}
						break;
					case 2:
						if(moveRight()){
							finishX--;
							direction = Direction.Right;
							get=true;
						}
						break;
					case 3:
						if(moveLeft()){
							finishX++;
							direction = Direction.Left;
							get=true;
						}
						break;
					}
					try{
						Thread.sleep(50);
					} catch(InterruptedException ie){}
				}
				
				if(direction.equals(Direction.Up)){
					while(existing){
						if(!Level.pause){
							if(y>stopY){
								--y;
								if(checkCollision()){
									player.lostLife();
								}
							} else{
								y = stopY;
								startY = finishY;
								
								if(moveUp()){
									stopX = Level.elementWidth*finishX;
									stopY = Level.elementHeight*finishY;
									continue;
								} else{
									break;
								}
							}
						}
						try{
							Thread.sleep((int)(wait/stepY));
						} catch(InterruptedException ie){}
					
					}
				} else if(direction.equals(Direction.Down)){
					while(existing){
						if(!Level.pause){
							if(y<stopY){
								++y;
								if(checkCollision()){
									player.lostLife();
								}
							} else{
								y = stopY;
								startY = finishY;
								
								if(moveDown()){
									stopX = Level.elementWidth*finishX;
									stopY = Level.elementHeight*finishY;
									continue;
								} else{
									break;
								}
							}
							
						}
						try{
							Thread.sleep((int)(wait/stepY));
						} catch(InterruptedException ie){}
					}
				} else if(direction.equals(Direction.Left)){
					while(existing){
						if(!Level.pause){
							if(x>stopX){
								--x;
								if(checkCollision()){
									player.lostLife();
								}
							} else{
								x = stopX;
								startX = finishX;
								if(moveLeft()){
									stopX = Level.elementWidth*finishX;
									stopY = Level.elementHeight*finishY;	
									continue;
								} else{
									break;
								}
							}
						}
						try{
							Thread.sleep((int)(wait/stepX));
						} catch(InterruptedException ie){}
					}
				} else if(direction.equals(Direction.Right)){				
					while(existing){
						if(!Level.pause){
							if(x<stopX){
								++x;
								if(checkCollision()){
									player.lostLife();
								}
							} else{
								x = stopX;
								startX = finishX;
								if(moveRight()){
									stopX = Level.elementWidth*finishX;
									stopY = Level.elementHeight*finishY;
									continue;
								} else{
									break;
								}
							}
						}
						try{Thread.sleep((int)(wait/stepX));}
						catch(InterruptedException ie){}
					}
				}
			}
			try{Thread.sleep(50);} 
			catch(InterruptedException ie){}
		}
		
	}
}
