package elements;

import game.GameBoard;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

/**
 * Klasa reprezentująca skrzynki
 * @author Dariusz Błaszkiewicz
 * @author Michał Winiarek
 */
public class Box extends GameElements{
		
	public Box(){
		InputStream file =getClass().getResourceAsStream("/"+cfg.getFromFile("#ImageBox"));
		try{
			picture = ImageIO.read(file);
		} catch(IOException e){
			System.err.println("Error");
			e.printStackTrace();
		}
	}
	
	public boolean isMovable(){
		return false;
	}
	
	public boolean isFireable(){
		return true;
	}
	
	public boolean isNextFireable(){
		return false;
	}
		
	public boolean isBox(){
		return true;
	}
	
	public boolean explosion(int x, int y){
		GameBoard.setElement(x, y, new Road());
		player.updateScore(cfg.getPointsBox());
		return true;
	}
	
	public boolean explosionToPortal(int x, int y){
		GameBoard.setElement(x, y, new Portal());
		player.updateScore(cfg.getPointsBox());
		return true;
	}
	
	public boolean explosionToBonusBombs(int x, int y){
		GameBoard.setElement(x, y, new BonusBombs());
		player.updateScore(cfg.getPointsBox());
		return true;
	}
	
	public boolean explosionToBonusRange(int x, int y){
		GameBoard.setElement(x, y, new BonusRange());
		player.updateScore(cfg.getPointsBox());
		return true;
	}
}
