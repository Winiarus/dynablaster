package elements;

import game.Config;

import java.io.File;

import javax.swing.ImageIcon;

/**
 * Klasa reprezentująca ogień
 * @author Dariusz Błaszkiewicz
 * @author Michał Winiarek
 */
public class Fire {
	
	Config cfg = Config.getInstance();
	ImageIcon imageIcon;
	
	private int x, y;
	private int realX, realY;
	
	/**
	 * konstruktor bezparametrowy
	 */
	public Fire(){
		try{
			imageIcon = new ImageIcon(getClass().getResource("/"+cfg.getFromFile("#ImageFire")));
		} catch(Exception e){
			System.err.println("Error");
			e.printStackTrace();
		}
	}
	
	public ImageIcon getIcon(){return imageIcon; }
	
	public int getX(){return x;}
	public int getY(){return y;}
	public int getRealX(){return realX;}
	public int getRealY(){return realY;}
	
	public void setPos(int x_, int y_){
		x=x_;
		y=y_;
	}
	public void setRealPos(int rx, int ry){
		realX = rx;
		realY = ry;
	}
}
